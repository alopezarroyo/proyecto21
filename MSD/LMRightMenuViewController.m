//
//  LMRightMenuViewController.m
//  LMSideBarControllerDemo
//
//  Created by LMinh on 10/11/15.
//  Copyright © 2015 LMinh. All rights reserved.
//

#import "LMRightMenuViewController.h"
#import "UIViewController+LMSideBarController.h"
#import "MainNC.h"

@interface LMRightMenuViewController ()

@property (nonatomic, strong) NSArray *menuTitles;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (weak, nonatomic) IBOutlet UIView *contentPerfil;

@end

@implementation LMRightMenuViewController


#pragma mark - VIEW LIFECYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.menuTitles = @[@"Inicio",@"Términos y Condiciones", @"Ubica tu farmacia" ,@"Preferencias",@"Ejercicios",];
    //@"Salir", @"Historial", @"Inscribete", @"Catálogo" 
    
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.frame.size.height/2;
    self.avatarImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarImageView.layer.borderWidth = 2.0f;
    self.avatarImageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.avatarImageView.layer.shouldRasterize = YES;
    
}

-(IBAction)showTapBtnPerfil:(id)sender {
 
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.contentPerfil.frame  = CGRectMake(80, 0, 300, self.view.frame.size.height);
        

    } completion:^(BOOL finished) {
        
    
        
        
    }];

    
}

-(IBAction)hideDetail:(id)sender{
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
      self.contentPerfil.frame  = CGRectMake(80, self.view.frame.size.height , 300,self.contentPerfil.frame.size.height);
      

    } completion:^(BOOL finished) {
    }];
}

-(IBAction)showId:(id)sender{
    
    MainNC *mainNavigationController = (MainNC *)self.sideBarController.contentViewController;
    [mainNavigationController showCuentaViewController];
    [self.sideBarController hideMenuViewController:YES];

}


#pragma mark - TABLE VIEW DATASOURCE

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.menuTitles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"menuCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    cell.textLabel.text = self.menuTitles[indexPath.row];
    cell.textLabel.textColor = [UIColor whiteColor];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}


#pragma mark - TABLE VIEW DELEGATE

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MainNC *mainNavigationController = (MainNC *)self.sideBarController.contentViewController;
    NSString *menuTitle = self.menuTitles[indexPath.row];
    if ([menuTitle isEqualToString:@"Inicio"]) {
        [mainNavigationController showHomeViewController];
    }
    else {
        
        if(indexPath.row==2){
            [mainNavigationController showGeoViewController];
        }else if(indexPath.row==5){
            [mainNavigationController showHistorialViewController];
        }else if(indexPath.row==4){
            [mainNavigationController showEjerciciosViewController];
        }else if(indexPath.row==7){
            [mainNavigationController showCatalogoViewController];
        }else if(indexPath.row==4){
            [mainNavigationController showRegisterVCController];
        }else if(indexPath.row==7){
            exit(10);
        }else if(indexPath.row==3){
            [mainNavigationController showPrefViewController];
        }else if(indexPath.row==1){
            mainNavigationController.othersViewController.titleStr = @"";
            [mainNavigationController showOthersViewController];
        }else{
          
        }
    }
    
    [self.sideBarController hideMenuViewController:YES];
}
@end
