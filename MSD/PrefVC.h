//
//  PrefVC.h
//  MSD
//
//  Created by Rene Cabañas Lopez on 11/01/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PrefVC : UIViewController<UITableViewDelegate,UITableViewDelegate>{
    NSString *titleStr;
    
}


@property(strong,nonatomic) NSString *titleStr;


@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
