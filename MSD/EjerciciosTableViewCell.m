//
//  EjerciciosTableViewCell.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 16/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "EjerciciosTableViewCell.h"

@implementation EjerciciosTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
