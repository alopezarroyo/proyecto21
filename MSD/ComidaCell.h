//
//  ComidaCell.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComidaCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblComidas;
@property (weak, nonatomic) IBOutlet UIImageView *imgComidas;
- (IBAction)actionButton:(UIButton *)sender;

@end
