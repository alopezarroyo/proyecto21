//
//  BaseVC.m
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "BaseVC.h"

@interface BaseVC ()

@end

@implementation BaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    logoImg=[[UIImageView alloc]initWithFrame:CGRectMake(10, 10, 79, 30)];
    logoImg.image=[UIImage imageNamed:@"logo"];
    menuBtn=[[UIButton alloc]initWithFrame:CGRectMake(370, 5, 40, 27)];
    [menuBtn setImage:[UIImage imageNamed:@"menuBtn"] forState:UIControlStateNormal];
    
    
    /*
    reloadBtn= [[UIButton alloc]initWithFrame:CGRectMake(285, 10, 20, 25)];
    reloadBtn.imageView.image=[UIImage imageNamed:@"btnreload"];
    [reloadBtn setImage:[UIImage imageNamed:@"btnreload"] forState:UIControlStateNormal];
    [reloadBtn addTarget:self action:@selector(reload) forControlEvents:UIControlEventTouchDown];
    
    */
}
-(void)viewDidAppear:(BOOL)animated
{
    if(fondoBlanco==nil)
    {
        fondoBlanco=[[UIView alloc]initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, self.view.frame.size.height)];
        fondoBlanco.backgroundColor=[UIColor yellowColor];
        [navBar addSubview:fondoBlanco];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
