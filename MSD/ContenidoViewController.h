//
//  ContenidoViewController.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContenidoViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSArray * titulo;
@property (strong, nonatomic) NSArray * descripcion;
@property (strong, nonatomic) NSArray * imgDesayunos;
@property (weak, nonatomic) IBOutlet UILabel *tituloComida;
@property (weak, nonatomic) IBOutlet UILabel *labelColacion;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *labelDado;



@property NSInteger *first;

@end
