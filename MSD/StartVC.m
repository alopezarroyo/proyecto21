//
//  StartVC.m
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "StartVC.h"
#import "JSONHelper.h"

@interface StartVC ()

@end

@implementation StartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(loadMainView) userInfo:nil repeats:NO];
    
    
}
-(void)viewDidAppear:(BOOL)animated
{
    if(isAppeared)
    {
        
    }
    else
    {
        isAppeared=YES;
        
        
    }
}
-(void)loadMainView
{
    MainNC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"MainNC"];
    [self presentViewController:vista animated:YES completion:^{
        NSLog(@"showIndex");
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
