//
//  RecetaViewController.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 26/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "RecetaViewController.h"
#import "UIViewController+LMSideBarController.h"

@interface RecetaViewController ()

@end

@implementation RecetaViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    
    if(_first == 0){
        if(_second == 0){
            self.imgReceta.image = [UIImage imageNamed:@"1_desayuno.png"];

            self.labelTitulo.text = @"Pan Pita y Fruta";
            self.textReceta.text = @"\nReceta\n\n1 pieza de pan pita pequeño con 40 gramos de queso panela y 2 rebanadas de jamón de pavo,\n1 rebanada de aguacate, jitomate y lechuga al gusto.\nAcompaña con: 1 manzana pequeña, 1 a 2 tazas de café o té.\n\n+OPCIONES\n\n- Si no tienes pan pita, sustitúyelo por 2 tortillas de nopal para hacer 2 quesadillas.\n- Si no tienes queso panela, sustitúyelo por 30 gramos de queso oaxaca o 30 gramos de queso manchego.\n- Si no tienes jamón de pavo puedes poner 40 gramos de salmón.\n- Si no tienes aguacate, sustitúyelo por 1 cucharadita de mayonesa. Puedes poner mostaza al gusto.La manzana la puedes sustituir por 1 pera pequeña o 2 naranjas.";
        }else if(_second == 1){
            self.imgReceta.image = [UIImage imageNamed:@"2_desayuno.png"];
            
            self.labelTitulo.text = @"Sándwich de Salmón";
            self.textReceta.text = @"Receta\n\n2 rebanadas de pan de caja integral tradicional,\n60 gramos de salmón ahumado acompañado con lechuga, jitomate y rebanadas de cebolla al gusto.\nAcompaña con: 2 cucharadas de guacamole, 2 mandarinas, 1 a 2 tazas de café o té\n\n+OPCIONES\n\n-	 Si no tienes pan integral, sustitúyelo por pan blanco.\n- Si no tienes salmón ahumado sustitúyelo por 60 gramos de pollo asado (pechuga de pollo).\n- Si no tienes lechuga puedes usar espinacas.\n-	 Si no tienes guacamole puedes untar 1 cucharada de queso crema o crema al pan.\n- Si no tienes mandarinas puedes comer 1 taza de papaya, 1 taza de melón, 1 taza de sandía.";
        }else if(_second == 2){
            self.imgReceta.image = [UIImage imageNamed:@"3_desayuno.png"];
            
            self.labelTitulo.text = @"	Sincronizada y Fruta";
            self.textReceta.text = @"Receta\n\n2 tortillas de nopal con 2 rebanadas de pechuga de pavo,\n 1 rebanada de queso panela de 40 gramos con 1/3 de aguacate y salsa de molcajete al gusto.\n Acompaña con: 3/4 de taza de zarzamoras, 1 a 2 tazas de café o té.\n\n+OPCIONES\n\n- 	Si no te gusta el queso panela, come sólo 30 gramos de queso oaxaca o manchego.\n- Si no te gustan las tortillas de nopal come sólo 1 tortilla de harina.\n- Si no te gusta el aguacate, pon sólo 1 cucharadita de crema.\n- La salsa de molcajete la puedes sustituir por salsa pico de gallo.";
        }else if(_second == 3){
            self.imgReceta.image = [UIImage imageNamed:@"4_desayuno.png"];
            
            self.labelTitulo.text = @"	Chapata de Salmón";
            self.textReceta.text = @"Receta\n\n1 pieza de chapata mediana sin migajón,\n 1cucharada de aguacate para untar,\n 60 gramos de salmón,\n lechuga para acompañar,\n cebolla al gusto.\nAcompaña con: 3 piezas de guayaba, 1 a 2 tazas de café o té.\n\n+OPCIONES\n\n- Puedes usar edulcorantes no calóricos en el café o té.\n-	 Si no tienes aguacate puedes poner 1 cucharada de queso crema.\n- Si no tienes salmón, pon 60 gramos de pollo asado o de queso panela.\n- Si ya te cansaste de la lechuga, prueba poniendo arúgula o espinacas para acompañar.\n-	Si no te gusta la cebolla, prueba con el jitomate o con zanahoria rallada en tu chapata.";

        }else if(_second == 4){
            self.imgReceta.image = [UIImage imageNamed:@"5_desayuno.png"];
            
            self.labelTitulo.text = @"	Huevos a la Mexicana";
            self.textReceta.text = @"Receta\n\n2 huevos revueltos con 3/4 de taza salsa pico de gallo,\n utilizando 1 cucharadita de aceite para cocinarlos y 1 bolillo pequeño.\nAcompaña con: 1 mango manila, 1 a 2 tazas de café o té.\n\n+OPCIONES\n\n- Si no te gustan las yemas, sustituye 1 huevo entero por 2 claras de huevo.\n- Si no tienes salsa pico de gallo, utiliza cualquier salsa casera hecha con jitomates y chiles.\n- Puedes sustituir el aceite por 5 disparos de spray de aceite tipo Pam.\n- Si no tienes el bolillo pequeño come 2 tortillas o 1 paquete de galletas Salmas.\nEl mango manila lo puedes sustituir con 1/2 mango petacón o con 1/2 plátano.";
        }
        else if(_second == 5){
            self.imgReceta.image = [UIImage imageNamed:@"6_desayuno.png"];
            
            self.labelTitulo.text = @"	Molletes con Pico de Gallo";
            self.textReceta.text = @"Receta\n\n2 mitades de bolillo mediano,\n1 cucharada de frijoles molidos para cada mitad,\n30 gramos de queso oaxaca para cada bolillo,\nsalsa pico de gallo al gusto.\nAcompaña con: Ensalada de lechuga con jitomate para acompañar, 2 duraznos.\n\n+OPCIONES\n\n-	 Puedes sustituir cada mitad de bolillo por 1 tortilla de maíz pequeña para unas quesadillas.\n- Los 30 gramos de queso oaxaca pueden sustituirse por 30 gramos de manchego o 40 gramos de queso panela.\n- Si no te gustan los frijoles, puedes untar 2 cucharadas de humus de garbanzo y sustituir el bolillo por 2 tostadas horneadas.";
        }else{
            self.imgReceta.image = [UIImage imageNamed:@"7_desayuno.png"];
            
            self.labelTitulo.text = @"Sándwich de Atún";
            self.textReceta.text = @"Receta\n\n2 rebanadas de pan de caja con 1/4 de lata de atún preparado en ensalada con verduras, mostaza al gusto.\nAcompaña con: 1 taza de melón,\n1 a 2 tazas de café o té.\n\n+OPCIONES\n\n- Puedes sustituir el melón por papaya, piña o sandía.\n- Puedes usar 1 bolillo pequeño en lugar de las 2 rebanadas de pan de caja.\n- Puedes sustituir cada rebanada de pan por 1 tostada y hacerte 2 tostadas.\n- Puedes sustituir el atún por jamón y queso.";
        }
    }else if(_first == 1){
        if(_second == 0){
            self.imgReceta.image = [UIImage imageNamed:@"1_comida.png"];
            
            self.labelTitulo.text = @"Fajitas con Verduras";
            self.textReceta.text = @"\nReceta\n\n120 gramos de fajitas de pollo sobre cama de verduras (para éstas es posible utilizar 1 cucharadita de mantequilla y pimienta o hierbas finas como sazonador),\n2 vasos de agua de limón con chía.\n\n*Se recomienda consumir 1 taza de verdura y 1/2 taza de arroz al vapor\n\n+OPCIONES\n\n1 cucharadita de mantequilla\n= 1 cucharadita de crema\n= 1 cucharadita de margarina\n= 1 cucharada de aderezo\n-	120 gramos de fajitas de pollo = 120 gramos de bistec de res asado = 120 gramos de carne molida.\n- Puedes sustituir 1 taza de verduras cocidas por 2 tazas de verduras frescas, p. ej., 2 tazas de pepino, 2 tazas de rábano, 2 tazas de pimientos o 2 tazas de espinacas.\n- Puedes sustituir 1/2 taza de arroz al vapor por 1 rebanada de pan de caja o por ½ taza de sopa de pasta cocida.\nUsa sólo una cucharada de chía por cada vaso de 250 ml.";
        }else if(_second == 1){
            self.imgReceta.image = [UIImage imageNamed:@"2_comida.png"];
            
            self.labelTitulo.text = @"Pescado con Ensalada";
            self.textReceta.text = @"\nReceta\n\n120 gramos de pescado con limón,\n2 tazas de ensalada de lechuga, espinacas y arúgula (1 taza de espinacas frescas con 5 jitomates cherry,\n1/2 taza de arúgula),\n2 vasos de agua de jamaica.\nAcompaña con: Aderezo de vinagre balsámico acompañado de 1/2 taza de macarrones.\n\n+OPCIONES\n\n- Puedes sustituir la ensalada fresca por verduras cocidas\n- 120 gramos de pescado = 120 gramos de trucha salmonada.\n- Si no tienes pescado o trucha, puedes sustituirlo por 1 lata de atún.\n- 1 taza de espinacas frescas = 1/2 chayote = ½ pieza de alcachofa = 1/2 taza de nopales cocidos.\n- 1/2 taza de macarrones = 1 pieza de papa, 1 elote pequeño = 1 rebanada de pan de caja.\n- 1 taza de flor de jamaica hervida; el jarabe que quede dilúyelo con 250 ml de agua.";
        }else if(_second == 2){
            self.imgReceta.image = [UIImage imageNamed:@"7_comida.png"];
            
            self.labelTitulo.text = @"Hamburguesa con ensalada";
            self.textReceta.text = @"Receta\n\n120 gramos de carne molida de res para hamburguesa,\n1/2 pieza de pan de hamburguesa (utiliza solo la base o la tapa).\nEnsalada de lechuga con pepino,\n2 vasos de agua de limón con menta.\nAcompaña con: limón y pimiento para aderezar..\n\n+OPCIONES\n\n- - Para la ensalada utiliza 1/3 de taza de pepino con cáscara rebanado y 2 tazas de lechuga.\n- 120 gramos de carne de res asada = 120 gramos de pechuga de pollo sin piel a la plancha.\n- Si hoy no quieres comer carne sustituye los 120 gramos de carne molida de res por 120 gramos de hamburguesa de soya utilizando soya texturizada.\n- Puedes sustituir el 1/3 de taza de pepino por 1/3 de taza de pepinillos crudos o por 1 cucharada de pepinillos dulces picados y la lechuga por 1/2 taza de pimiento cocido para acompañar tu hamburguesa.\n- A 1 vaso de agua de limón se le agregan hojas de menta.";
            
            
        }else if(_second== 3){
            self.imgReceta.image = [UIImage imageNamed:@"3_comida.png"];
            
            self.labelTitulo.text = @"Ensalada de Atún";
            self.textReceta.text = @"\nReceta\n\n1 lonja de atún de 120 gramos con jitomate, cebolla y limón;\n1/2 taza de arroz cocido,\nensalada de 2 tazas de lechuga con 1/2 jitomate y cebolla al gusto,\n2 vasos de agua de limón y pepino.\n*Aderezar con una vinagreta\n\n+OPCIONES\n\n- Puedes sustituir 120 gramos de lonja de atún con 1 lata de atún en agua.\n- 2 tazas de lechuga = 2 tazas de flor de calabaza = 2 tazas de espárragos cocidos = 1 taza de berenjenas cocidas.\n- A 1 taza de agua de limón se le agregan 3 o 4 rodajas de pepino.";
            
            
            
        }else if(_second == 4){
            self.imgReceta.image = [UIImage imageNamed:@"4_comida.png"];
            
            self.labelTitulo.text = @"Ceviche de Pescado";
            self.textReceta.text = @"\nReceta\n\n1 taza de ceviche de pescado con jitomate, cebolla y chile picado en pedazos pequeños,\n2 tostadas horneadas,\n2 vasos de agua de jamaica con menta.\nAcompañar: 1 cucharada de aceite de oliva por taza de ceviche.\n\n+OPCIONES\n\n- 2 tostadas horneadas = 1 paquete de galletas Salmas = 1/2 taza de arroz cocido.\n- 1 taza de flor de jamaica hervida; al jarabe que quede después de colar se le añaden 250 ml de agua y hojas de menta.";
            
            
            
        }else if(_second == 5){
            self.imgReceta.image = [UIImage imageNamed:@"5_comida.png"];
            
            self.labelTitulo.text = @"Pescado con Arroz";
            self.textReceta.text = @"\nReceta\n\n1120 gramos de pescado sarandeado,\n1/2 taza de arroz cocido,\n2 tazas de ensalada de verdura fresca,\n2 vasos de agua de limón con chía.\n*Acompañar con 1 cucharada de aderezo.\n\n+OPCIONES\n\n- Sustituye 2 tazas de verdura fresca por 1 taza o 1 1/2 taza de verdura cocida.\n- 1/2 pieza de bolillo sin migajón = 1/2 taza de arroz cocido = 1 pieza de pan de caja = 1 tortilla.\n- Usa sólo una cucharada de chía por cada vaso de 250 ml.";
            
            
         
        }else{
            self.imgReceta.image = [UIImage imageNamed:@"6_comida.png"];
            
            self.labelTitulo.text = @"Pechuga Empanizada";
            self.textReceta.text = @"\nReceta\n\n120 gramos de pechuga de pollo empanizada (utilizar 2 cucharadas de pan molido) con 1 taza de rebanadas de pepino,\n1/4 de taza de rábanos,\n2 vasos de agua de jamaica.\n* Acompañar con jugo de limón, aceite de oliva y vinagre.\n\n+OPCIONES\n\n- Si no quieres pechuga de pollo puedes sustituirla por 120 gramos de filete de res empanizado.\n- Si no quieres empanizar la pechuga puedes dejarla asada y comer en lugar del pan molido 1 tortilla o ponerle croutones a la ensalada.\n- 1 taza de flor de jamaica hervida; el jarabe que quede dilúyelo con 250 ml de agua.";
        }
        
    }else if(_first == 2){
        if(_second == 0){
            self.imgReceta.image = [UIImage imageNamed:@"1_cena.png"];
            
            self.labelTitulo.text = @"Quesadillas de Champiñones";
            self.textReceta.text = @"Receta\n\nQuesadilla de tortilla de maíz con 40 gramos de queso panela y 3/4 de taza de champiñón cocido.\nAcompaña con: 1 taza de fruta, 1 taza de té.\n\n+OPCIONES\n\n- 40 gramos de queso panela = 30 gramos de queso Oaxaca.\n- En lugar de los champiñones puedes poner a tu quesadilla 1/2 taza de salsa pico de gallo o poner lechuga y jitomate.\n- Si te gusta el plátano o el mango recuerda que 1 taza de fruta = 1/2 mango o 1/2 plátano.";
        }else if(_second == 1){
            self.imgReceta.image = [UIImage imageNamed:@"2_cena.png"];
            
            self.labelTitulo.text = @"Cereal con Fruta";
            self.textReceta.text = @"Receta\n\n1 taza de cereal alto en fibra sin azúcar con 1 taza de leche,\n1 taza de fruta.\nTip: Utiliza leche light o deslactosada para acompañar el cereal.\n\n+OPCIONES\n\n- Puedes sustituir la taza de leche por ¾ de taza de yogurt natural.\n- Puedes sustituir la taza de cereal por 2 cucharadas de granola o 1/2 taza de avena.";
        }else if(_second == 2){
            self.imgReceta.image = [UIImage imageNamed:@"3_cena.png"];
            
            self.labelTitulo.text = @"Pan Pita con Jocoque";
            self.textReceta.text = @"Receta\n\n1 pieza de pan pita con 4 cucharadas de jocoque seco untado,\n2 rebanadas de jamón.\nAcompaña con: Ensalada de verdura fresca al gusto, 1 taza de fresas.\n\n+OPCIONES\n\n- Puedes sustituir 1 pieza de pan pita por 1 taza de cereal alto en fibra.\n- Puedes sustituir 4 cucharadas de jocoque seco por 3/4 de taza de yogurt natural y comer cereal con yogurt.\n- 1 taza de fresas = 1 taza de papaya = 1/2 pera = 1/2 plátano tabasco = 1 1/2 kiwi.\n- Puedes sustituir el jamón por 30 gramos de salmón ahumado para tu pan pita con jocoque.";
        }else if(_second == 3){
            self.imgReceta.image = [UIImage imageNamed:@"4_cena.png"];
            
            self.labelTitulo.text = @"Huevo con Ensalada";
            self.textReceta.text = @"Receta\n\n1 huevo estrellado con ensalada al gusto o revuelto con verduras,\n1 tortilla y 1 licuado de fresa.\nAcompañar con: 1 taza de leche descremada o light y 1 taza de fresas para el licuado.\n\n+OPCIONES\n\n- Puedes sustituir la taza de fresas por 1/2 plátano.\n- Puedes sustituir 1 taza de leche por 1 taza de leche de soya o de almendra";
        }else if(_second == 4){
            self.imgReceta.image = [UIImage imageNamed:@"5_cena.png"];
            
            self.labelTitulo.text = @"Huevo con Champiñones";
            self.textReceta.text = @"Receta\n\n1 huevo revuelto al gusto,\n3/4 de taza de champiñones,\n1/2 bolillo sin migajón.\nAcompaña con: 1 pieza de fruta.\n\n+OPCIONES\n\n- Si no te gusta el huevo entero, puedes sustituirlo por 2 claras.\n- Si no tienes champiñones frescos, puedes usar champiñones en lata rebanados.\n- 1 huevo entero = 1 salchicha de pavo.";
        }else if(_second == 5){
            self.imgReceta.image = [UIImage imageNamed:@"6_cena.png"];
            
            self.labelTitulo.text = @"Ensalada Capresse";
            self.textReceta.text = @"Receta\n\n1 jitomate en rebanadas,\n30 gramos de queso feta,\nensalada al gusto,\n1 cucharadita de aceite de oliva y limón o vinagreta como aderezo.\nAcompañar con: 1/2 bolillo sin migajón, ¾ de piezas de guanábana.\n\n+OPCIONES\n\n- Puedes sustituir la cucharadita de aceite de oliva por algún otro aderezo a base de aceite.\n- Puedes sustituir los 30 gramos de queso feta por 30 gramos de queso de cabra o 30 gramos de queso panela.\n- Puedes sustituir el bolillo por 2 cucharadas de croutones para tu ensalada.";
        }else{
            self.imgReceta.image = [UIImage imageNamed:@"7_cena.png"];
            
            self.labelTitulo.text = @"Papaya con Yogurt";
            self.textReceta.text = @"Receta\n\n1 taza de papaya con 3/4 de taza de yogurt y 1 quesadilla.\nAcompañar con: 1 tortilla de maíz con 30 gramos de queso Oaxaca\n\n+OPCIONES\n\n- Si ya te cansaste del queso, puedes hacer un taco con tu tortilla sustituyendo el queso por 2 o 3 cucharadas de algún guisado.\n- Otras frutas que puedes combinar con el yogurt son 3 piezas de ciruelas, 3 piezas de dátiles, 1/3 de pieza de mamey y 1/2 taza de uvas.";
        }
        
    }else{
        
    }
    self.navigationItem.hidesBackButton = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sideMenu:(UIBarButtonItem *)sender {
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}

- (IBAction)backButton:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
