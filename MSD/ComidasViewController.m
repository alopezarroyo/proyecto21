//
//  ComidasViewController.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "ComidasViewController.h"
#import "ComidaCell.h"
#import "ContenidoViewController.h"
#import "UIViewController+LMSideBarController.h"


@interface ComidasViewController ()

@end

@implementation ComidasViewController

int first = 0;


- (IBAction)sideMenuAction:(UIBarButtonItem *)sender {
     [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
    
}

- (IBAction)actionButton:(UIButton *)sender {
     //[self performSegueWithIdentifier:@"segueToContenido" sender:self];
}

- (IBAction)backBTN:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.comidas = @[@"DESAYUNOS",@"COMIDAS",@"CENAS",@"COLACIONES"];
    self.imagenComidas = @[@"desayuno_1",@"comida_1",@"cena_1",@"colaciones"];
    self.navigationItem.leftBarButtonItem.enabled = nil;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.comidas count];
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int indice = (int)indexPath.row;
    
    static NSString *cellIdentifier = @"cell";
    ComidaCell *cell = (ComidaCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblComidas.text = self.comidas[indice];
    cell.imgComidas.image = [UIImage imageNamed:self.imagenComidas[indice]];
     cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    first = (int)indexPath.row;
    printf("%d", first);
    
    [self performSegueWithIdentifier:@"segueToContenido" sender:self];
    
    
}



- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"segueToContenido"])
    {
   ContenidoViewController *controller = (ContenidoViewController *)segue.destinationViewController;
        controller.first = first;
        //if you need to pass data to the next controller do it here
    }
}


@end
