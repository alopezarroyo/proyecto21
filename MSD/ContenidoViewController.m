//
//  ContenidoViewController.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "ContenidoViewController.h"
#import "ContenidoCell.h"
#import "RecetaViewController.h"
#import "UIViewController+LMSideBarController.h"


@interface ContenidoViewController ()

@end

@implementation ContenidoViewController

int second = 0;
//int first = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    if(_first == 0)
    {
        self.tituloComida.text = @"DESAYUNOS";
        self.titulo = @[@"Pan Pita y Fruta",@"Sándwich de Salmón",@"Sincronizada y Fruta",@"Chapata de Salmón",@"Huevos a la Mexicana",@"Molletes con Pico de Gallo",@"Sándwich de Atún"];
        self.descripcion = @[@"1 pieza de pan pita pequeño con 40 gramos de queso panela y 2 rebanadas de jamón de pavo 1 rebanada de aguacate, jitomate...",@"2 rebanadas de pan de caja integral tradicional, 60 gramos de salmón ahumado acompañado con lechuga, jitomate...",@"2 tortillas de nopal con 2 rebanadas de pechuga de pavo, 1 rebanada de queso panela de 40 gramos con 1/3...",@"1 pieza de chapata mediana sin migajón, 1 cucharada de aguacate para untar, 60 gramos de salmón, lechuga para acompañar...",@"2 huevos revueltos con 3/4 de taza de salsa pico de gallo, utilizando 1 cucharada de aceite para cocinarlos y 1 bolillo...",@"2 mitades de bolillo mediano, 1 cucharada de frijoles molidos para cada mitad, 30 gramos de queso oaxaca para cada bolillo...",@"2 rebanadas de pan de caja con 1/4 de lata de atún preparado en ensalada con verduras, mostaza al gusto..."];
        self.imgDesayunos = @[@"desayuno_1",@"desayuno_2",@"desayuno_3",@"desayuno_4",@"desayuno_5",@"desayuno_6",@"desayuno_7"];
        self.labelColacion.hidden = true;
        self.scrollView.hidden = true;
        self.labelDado.hidden = true;
    }
    else if(_first == 1)
    {
        self.tituloComida.text = @"COMIDAS";
        self.titulo = @[@"Fajitas con Verduras",@"Pescado con Ensalada",@"Hamburguesa con Ensalada",@"Ensalada de Atún",@"Ceviche de Pescado",@"Pescado con Arroz",@"Pechuga Empanizada"];
        self.descripcion = @[@"120 gramos de fajitas de pollo sobre cama de verduras (para éstas es posible utilizar 1 cucharadita de mantequilla y pimienta...", @"120 gramos de pescado con limón, 2 tazas de ensalada de lechuga, espinacas y arúgula (1 taza de espinacas frescas con 5 jitomates cherry, 1/2 taza de arúgula...", @"120 gramos de carne molida de res para hamburguesa, 1/2 pieza de pan de hamburguesa (utiliza solo la base o la tapa)...", @"! lonja de atún de 120 gramos con jitomate, cebolla y limón: 1/2 taza de arroz cocido, ensalada de 2 tazas de lechuga con 1/2 jitomate y cebolla al gusto, 2 vasos de ..." , @"1 taza de ceviche de pescado con jitomate, cebolla y chile picado en pedazos pequeños, 2 tostadas horneadas..." , @"120 gramos de pescado sarandeado, 1/2 taza de arroz cocido, 2 tazas de ensalada de verdura fresca, 2 vasos de agua de limón..." , @"120 gramos de pechuga de pollo empanizada (utilizar 2 cucharadas de pan molido) con 1 taza de rebanadas de pepino, 1/4 de...."];
        self.imgDesayunos = @[@"comida_1",@"comida_2",@"comida_7",@"comida_3",@"comida_4",@"comida_5",@"comida_6"];
        self.labelColacion.hidden = true;
        self.scrollView.hidden = true;
        self.labelDado.hidden = true;
    }
    else if( _first == 2)
    {
        self.tituloComida.text = @"CENAS";
        self.titulo = @[@"Quesadillas de Champiñones" , @"Cereal con Fruta" , @"Pan Pita con Jocoque" , @"Huevo con Ensalada",@"Huevo con Champiñones", @"Ensalada Capresse",@"Papaya con Yogurt"];
        
        self.descripcion = @[@"Quesadilla de tortilla de maíz con 40 gramos de queso panela y 3/4 de taza de champiñón cocido..." , @"1 taza de cereal alto en fibra sin azúcar con 1 taza de leche, 1 taza de fruta. Tip: Utiliza leche light o deslactosada para acompañar el cereal..." , @"1 pieza de pan pita con 4 cucharadas de jocoque seco untado, 2 rebanadas de jamón. Acompaña con: Ensalada de verdura fresca al gusto, 1 taza de fresas..." , @"1 huevo estrellado con ensalada al gusto o revuelto con verduras, 1 tortilla y 1 licuado de fresa..." , @"1 huevo revuelto al gusto, 3/4 de taza de champiñones, 1/2 bolillo sin migajón. Acompaña con: ! pieza de fruta..." , @"1 jitomate en rebanadas, 30 gramos de queso feta, ensalada al gusto. 1 cucharada de aceite de oliva y limón o vinagreta como..." , @"1 taza de papaya con 3/4 de taza de yogurt y 1 quesadilla. Acompañar con: 1 tortilla de maíz con 30 gramos de queso Oaxaca..."];
        
        self.imgDesayunos = @[@"cena_1",@"cena_2",@"cena_3",@"cena_4",@"cena_5",@"cena_6",@"cena_7"];
        self.labelColacion.hidden = true;
        self.scrollView.hidden = true;
        self.labelDado.hidden = true;
    }
    else
    {
        self.scrollView.contentSize = CGSizeMake(300, 900);
        self.scrollView.scrollEnabled = YES;
//        self.scrollView.frame = CGRectMake(0, 0, 320.0f, 480.0f);
//        self.scrollView.contentSize =CGSizeMake(1000.0F, 1000.0F);
        self.tituloComida.text = @"COLACIONES";
        self.labelColacion.text =@"Para la colación de medio día puedes escoger: 1 alimento del grupo de fruta/cereal + 1 de alimento del grupo de oleaginosas.";
        /* executes when the none of the above condition is true */
    }
    self.navigationItem.hidesBackButton = YES;


    printf("Primer variavle first\t");
    printf("%d", _first);
    printf("\n");
    

}

- (IBAction)sideMenu:(UIBarButtonItem *)sender {
     [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}



- (IBAction)backButton:(UIButton *)sender {
   [self dismissViewControllerAnimated:YES completion:nil];
    //[self performSegueWithIdentifier:@"segueToMenu" sender:self];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Metodos de UITableView DataSource

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.titulo count];
    
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int indice = (int)indexPath.row;
    
    static NSString *cellIdentifier = @"CellDesayuno";
    ContenidoCell *cell = (ContenidoCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    cell.lblTitulo.text = self.titulo[indice];
    cell.lblDescripcion.text = self.descripcion[indice];
    cell.imgDesayunos.image = [UIImage imageNamed:self.imgDesayunos[indice]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    second = (int)indexPath.row;
    printf("%d", second);
    printf("\n");
    
    [self performSegueWithIdentifier:@"segueToReceta" sender:self];
    
    //        RecetarioViewController *myVController = (RecetarioViewController *)[self.storyboard instantiateViewControllerWithIdentifier:@"Contenido"];
    //        [self.navigationController pushViewController:myVController animated:YES];
    
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"segueToReceta"])
    {
        RecetaViewController *controller = (RecetaViewController *)segue.destinationViewController;
        controller.first = _first;
        controller.second = second;
        
        //if you need to pass data to the next controller do it here
    }
}


@end
