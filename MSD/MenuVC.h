//
//  MenuVC.h
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseVC.h"
#import "MenuV.h"


@interface MenuVC : BaseVC <UITableViewDelegate,UITableViewDelegate>
{
    MenuV *menuV;
    int btnValue;
}
@property(nonatomic,strong)IBOutlet UIView *menuVista;

-(IBAction)showMenu:(id)sender;
-(IBAction)perfil:(id)sender;
-(IBAction)geoloc:(id)sender;
-(IBAction)catalogo:(id)sender;
-(IBAction)registro:(id)sender;
-(IBAction)cuenta:(id)sender;
-(IBAction)historial:(id)sender;

@end
