//
//  GeoLocVC.h
//  MSD
//
//  Created by Lightsoft on 16/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>

@interface GeoLocVC :  UIViewController  <MKMapViewDelegate,UITextFieldDelegate,CLLocationManagerDelegate>{
    
    NSString *titleStr;
    
}
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, strong) CLLocation* currentLocation;

@property(strong,nonatomic) NSString *titleStr;
@property (nonatomic, strong) IBOutlet MKMapView *mapView;
@property (nonatomic, strong) IBOutlet UITextField *textFieldSearch;
@property (strong, nonatomic) IBOutlet UIView *detailView;
@property (strong, nonatomic) IBOutlet UIImageView *imageDetail;
@property (strong, nonatomic) IBOutlet UITextView *textDetail;
@property(strong, nonatomic) NSArray *descriptionArray;
@property (strong, nonatomic) CLLocation *initialLocation;


- (IBAction)onSearchPressed:(id)sender;


@end
