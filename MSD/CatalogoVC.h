//
//  CatalogoVC.h
//  MSD
//
//  Created by Lightsoft on 16/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatalogoVC : UIViewController  <UIScrollViewDelegate>{

    NSString *titleStr;
    
}


@property(strong,nonatomic) NSString *titleStr;
- (void)pageChanged ;


@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UIPageControl *pageControl;

@end
