//
//  CustomAnnotation.h
//  MSD
//
//  Created by Fernando on 20/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#ifndef CustomAnnotation_h
#define CustomAnnotation_h


#endif /* CustomAnnotation_h */

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (copy, nonatomic) NSString *title;

-(id)initWithTitle:(NSString *)newTitle Location:(CLLocationCoordinate2D)location;
-(MKAnnotationView *) annotationView;

@end
