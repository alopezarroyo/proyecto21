//
//  InfirmacionInicialViewController.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 10/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "InfirmacionInicialViewController.h"
#import "UIViewController+LMSideBarController.h"

@interface InfirmacionInicialViewController ()

@end

@implementation InfirmacionInicialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.scrollView.contentSize = CGSizeMake(300, 900);
    self.scrollView.scrollEnabled = YES;
    self.navigationItem.hidesBackButton = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)continuarBTN:(id)sender {
    [self performSegueWithIdentifier:@"segueToComidas" sender:self];
}

- (IBAction)sideMenuAction:(UIBarButtonItem *)sender {
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
    
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
