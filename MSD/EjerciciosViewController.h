//
//  EjerciciosViewController.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 16/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EjerciciosViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollViewData;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (weak, nonatomic) IBOutlet UILabel *textLabel;

@property (nonatomic, strong) IBOutlet UILabel *ddText;
@property (nonatomic, strong) IBOutlet UIView *ddMenu;
@property (nonatomic, strong) IBOutlet UIButton *ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender;
- (IBAction)ddMenuSelectionMade:(UIButton *)sender;

@property (strong, nonatomic) NSArray * ejercicios;
@property (strong, nonatomic) NSArray * ejerciciosDescription;
@property (strong, nonatomic) NSArray * imagenEjercicios;

@property (strong, nonatomic) NSArray * ejerciciosA;
@property (strong, nonatomic) NSArray * ejerciciosDescriptionA;
@property (strong, nonatomic) NSArray * imagenEjerciciosA;

@property (strong, nonatomic) NSArray * ejerciciosB;
@property (strong, nonatomic) NSArray * ejerciciosDescriptionB;
@property (strong, nonatomic) NSArray * imagenEjerciciosB;

@property (weak, nonatomic) IBOutlet UITableView *tableViewData;
@property (weak, nonatomic) IBOutlet UIView *ejerciciosView;
@property (weak, nonatomic) IBOutlet UIView *twoEjerciciosView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewTwo;
@property (weak, nonatomic) IBOutlet UIView *threeView;
@property (weak, nonatomic) IBOutlet UITableView *tableViewThree;





@end
