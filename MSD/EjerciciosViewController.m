//
//  EjerciciosViewController.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 16/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "EjerciciosViewController.h"
#import "IGLDropDownMenu.h"
#import "EjerciciosTableViewCell.h"
#import "TwoTableView.h"
#import "ThreeTableView.h"
#import "UIViewController+LMSideBarController.h"


@interface EjerciciosViewController () <IGLDropDownMenuDelegate>

@property (nonatomic, copy) NSArray *dataArray;



@end

@implementation EjerciciosViewController

int indexTable = 0;
int value = 0;
int value2 = 0;
int value3 = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    // Do any additional setup after loading the view.
   
          self.ejercicios = @[@"Ejercicio 1",@"Ejercicio 2",@"Ejercicio 3",@"Ejercicio 4",@"Ejercicio 5",@"Ejercicio 6"];
          self.ejerciciosDescription = @[@"Eleva muslos alternando las piernas.",@"Haz círculos con los brazos alternando de lado y finalizando con ambos brazos al mismo tiempo.",@"Haz círculos con tu cabeza en ambas direcciones, lenta y suavemente.",@"Estando de pie, gira suavemente tu cintura de un lado al otro.",@"Estando de pie, con los pies a la altura de los hombres, extiende tu brazo hacia arriba alternando cada lado.",@"Con los pies juntos, sin que afecte tu equilibrio, sube y baja con las puntas de los pies, como si trataras de alcanzar un objeto. Puedes subir los brazos dependiendo cuánto equilibrio tengas."];
          self.imagenEjercicios = @[@"ejercicio_1",@"ejercicio_2",@"ejercicio_3",@"ejercicio_4",@"ejercicio_3",@"ejercicio_1"];
    
    
          self.ejerciciosA = @[@"Ejercicio 1",@"Ejercicio 2",@"Ejercicio 3"];
          self.ejerciciosDescriptionA = @[@"Recuéstate sobre el piso boca arriba, eleva tus piernas a 45⁰ y realiza movimientos tipo pedaleo de bicicleta.",@"Colócate de pie con pies juntos y realiza pequeños saltos sobre la puntas.",@"Coloca tus pies a la altura de los hombros, y dependiendo de tu fortaleza y comodidad, ve flexionando tu cuerpo hacia adelante con los brazos abiertos. Hazlo con mucho cuidado para no perder el equilibrio."];
          self.imagenEjerciciosA = @[@"ejercicio_1",@"ejercicio_2",@"ejercicio_3"];
    
    
        self.ejerciciosB = @[@"Ejercicio 1",@"Ejercicio 2",@"Ejercicio 3"];
        self.ejerciciosDescriptionB = @[@"Estando de píe,  flexiona una pierna por lado, tocando con la mano contraria. Ten mucho cuidado de no perder tu equilibrio.",@"Camina en círculos realizando respiraciones profundas.",@"Relaja y respira profundamente."];
        self.imagenEjerciciosB = @[@"ejercicio_1",@"ejercicio_4",@"ejercicio_2"];
    
    self.ddMenu.hidden = YES;
    self.ejerciciosView.hidden = YES;
    //self.twoEjerciciosView.hidden = YES;
     //self.navigationItem.hidesBackButton = YES;
    
    self.scrollViewData.contentSize = CGSizeMake(300, 900);
    self.scrollViewData.scrollEnabled = YES;
    
    self.tableViewData.delegate = self;
    self.tableViewData.dataSource = self;
    
    self.tableViewTwo.delegate = self;
    self.tableViewTwo.dataSource = self;
    
    self.tableViewThree.delegate = self;
    self.tableViewThree.dataSource = self;
    
    self.tableViewThree.hidden = YES;
    self.ejerciciosView.hidden = YES;
    self.twoEjerciciosView.hidden = YES;
    
    //[self.tableViewData reloadData];
   // [self.tableViewData.subviews reloadData];
    //NSLog(@"%@", self.tableViewData);
//    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
//    [refreshControl addTarget:self action:@selector(refreshTable:) forControlEvents:UIControlEventValueChanged];
//    [_tableViewData addSubview:refreshControl];
//    value = 1;
//    
    
}

//-(void)refreshTable {
//    [self.tableViewData reloadData];
//    if (self.refreshControl) {
//        
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"MMM d, h:mm a"];
//        NSString *title = [NSString stringWithFormat:@"Last update: %@", [formatter stringFromDate:[NSDate date]]];
//        NSDictionary *attrsDictionary = [NSDictionary dictionaryWithObject:[UIColor whiteColor]
//                                                                    forKey:NSForegroundColorAttributeName];
//        NSAttributedString *attributedTitle = [[NSAttributedString alloc] initWithString:title attributes:attrsDictionary];
//        self.refreshControl.attributedTitle = attributedTitle;
//        
//        [self.refreshControl endRefreshing];
//    }
//}

-(void)viewWillAppear:(BOOL)animated{
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAlertView{

    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"CALENTAMIENTO" message: @"Inicia siempre el calentamiento de entre 5 a 10 minutos con ejercicios que involucren movimiento de cabeza, cuello, hombros, brazos, cadera y piernas." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    UIImage* imgMyImage = [UIImage imageNamed:@"pesita.png"];
    UIImageView* ivMyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgMyImage.size.width, imgMyImage.size.height)];
    [ivMyImageView setImage:imgMyImage];
    
    [alert setValue: ivMyImageView forKey:@"accessoryView"];
    [alert show];
}

-(void)addAlertViewB{

    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"FASE AERÓBICA" message: @"Continúa con 10 a 40 minutos de ejercicios más intensos. Te recomendamos ir incrementando este tiempo paulatinamente, esto te ayudará a mejorar tu resistencia y el funcionamiento del corazón y los pulmones." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    UIImage* imgMyImage = [UIImage imageNamed:@"pesita.png"];
    UIImageView* ivMyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgMyImage.size.width, imgMyImage.size.height)];
    [ivMyImageView setImage:imgMyImage];
    
    [alert setValue: ivMyImageView forKey:@"accessoryView"];
    [alert show];
}

-(void)addAlertViewC{
    
    UIAlertView* alert = [[UIAlertView alloc] initWithTitle: @"FASE DE ENFRIAMIENTO Y RECUPERACIÓN" message: @"Termina tu sesión de ejercicios siempre, de 5 a 10 minutos, disminuyendo paulatinamente la intensidad, alternando con ejercicios suaves y de flexibilidad, con respiraciones profundas y lentas para desacelerar el ritmo cardiaco y pulmonar." delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    
    UIImage* imgMyImage = [UIImage imageNamed:@"pesita.png"];
    UIImageView* ivMyImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, imgMyImage.size.width, imgMyImage.size.height)];
    [ivMyImageView setImage:imgMyImage];
    
    [alert setValue: ivMyImageView forKey:@"accessoryView"];
    [alert show];
}

- (IBAction)sideMenuAction:(UIBarButtonItem *)sender {
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
    
}


@synthesize ddMenu, ddText;
@synthesize ddMenuShowButton;

- (IBAction)ddMenuShow:(UIButton *)sender
{
    if (sender.tag == 0) {
        sender.tag = 1;
        self.ddMenu.hidden = NO;
         //[self.tableViewData reloadData];
        [sender setTitle:@"" forState:UIControlStateNormal];
    } else {
        sender.tag = 0;
        self.ddMenu.hidden = YES;
         //[self.tableViewData reloadData];
        [sender setTitle:@"" forState:UIControlStateNormal];
    }
}


- (IBAction)ddMenuSelectionMade:(UIButton *)sender
{
    self.ddText.text = sender.titleLabel.text;
    [self.ddMenuShowButton setTitle:@"" forState:UIControlStateNormal];

    self.ddMenuShowButton.tag = 0;
    self.ddMenu.hidden = YES;
    switch (sender.tag) {
        case 1:
            
          
            [self addAlertView];
           self.ejerciciosView.hidden = NO;
            self.twoEjerciciosView.hidden = YES;
            self.scrollViewData.hidden = YES;
            self.tableViewThree.hidden = YES;
            
           
            break;
        case 2:
            
          
          [self addAlertViewB];
            self.tableViewThree.hidden = YES;
            self.ejerciciosView.hidden = YES;
            self.twoEjerciciosView.hidden = NO;
             self.scrollViewData.hidden = YES;
         
            
            break;
        case 3:

            
           [self addAlertViewC];
            self.tableViewThree.hidden = NO;
            self.twoEjerciciosView.hidden = YES;
             self.ejerciciosView.hidden = YES;
             self.scrollViewData.hidden = YES;
           
            break;
            

        default:
            break;
    }
}





-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
//    if(ddMenuShowButton.tag == 1){
//        return [self.ejercicios count];
//    }else if(ddMenuShowButton.tag == 2){
//        return [self.ejerciciosA count];
//    }else{
//        return [self.ejercicios count];
//    }
    
    if (tableView == self.tableViewData) {
        return [self.ejercicios count];
    }
    
    if (tableView == self.tableViewTwo) {
        return [self.ejerciciosA count];
    }
    
    if (tableView == self.tableViewThree) {
        return [self.ejerciciosB count];
    }
    
    return [self.ejercicios count];
    
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    int indice = (int)indexPath.row;
    //self.ddMenuShowButton.tag = 0;
    //[self refreshTable];
    
    if (tableView == self.tableViewData) {
        static NSString *cellIdentifier = @"cell";
        EjerciciosTableViewCell *cell = (EjerciciosTableViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        
        
        cell.descriptionLabel.text = self.ejerciciosDescription[indice];
        cell.titleEjercicio.text = self.ejercicios[indice];
        cell.ejercicioImage.image = [UIImage imageNamed:self.imagenEjercicios[indice]];

        return cell;
    }
    
    if (tableView == self.tableViewTwo) {
            static NSString *cellIdentifier1 = @"cell1";
            TwoTableView *cell1 = (TwoTableView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        
            cell1.labelDescription.text = self.ejerciciosDescriptionA[indice];
            cell1.labelTitle.text = self.ejerciciosA[indice];
            cell1.imageA.image = [UIImage imageNamed:self.imagenEjerciciosA[indice]];
        
        return cell1;
    }
    
    if (tableView == self.tableViewThree){
        static NSString *cellIdentifier2 = @"cell2";
         ThreeTableView *cell2 = (ThreeTableView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier2];
        
        cell2.labelDescriptionA.text = self.ejerciciosDescriptionB[indice];
        cell2.labelTitleA.text = self.ejerciciosB[indice];
        cell2.imageThree.image = [UIImage imageNamed:self.imagenEjerciciosB[indice]];
        
        return cell2;
    }
   
    
    
//    static NSString *cellIdentifier1 = @"cell1";
//    TwoTableView *cell1 = (TwoTableView *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
//    
//    cell1.labelDescription.text = self.ejerciciosDescriptionA[indice];
//    cell1.labelTitle.text = self.ejerciciosA[indice];
//    cell1.imageA.image = [UIImage imageNamed:self.imagenEjerciciosA[indice]];


    
    return nil;
    //return cell1;
  
    
}

-(void)tableViewDidLoadRows:(UITableView *)tableView{
    // do something after loading, e.g. select a cell.
    //[self.tableViewData reloadData];
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    first = (int)indexPath.row;
//    printf("%d", first);
    
   // [self performSegueWithIdentifier:@"segueToContenido" sender:self];
    
    
}




/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end


