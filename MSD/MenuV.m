//
//  MenuV.m
//  MSD
//
//  Created by Lightsoft on 15/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "MenuV.h"

@implementation MenuV

-(void)show
{
    CGFloat width=self.frame.size.width;
    CGFloat height=self.frame.size.height;
    UIView *backV=[[UIView alloc]initWithFrame:CGRectMake(0, 0, self.frame.size.width, height)];
    backV.backgroundColor=[UIColor blackColor];
    backV.alpha=0.6;
    UIView *contentV=[[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width*0.14, 0, self.frame.size.width*0.86, height)];
    contentV.backgroundColor=[UIColor whiteColor];
    [self addSubview:backV];
    [self addSubview:contentV];
    
    UIImageView *coverImg=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 356, 155)];
    //356x155
    coverImg.image=[UIImage imageNamed:@"cover.jpg"];
    [contentV addSubview:coverImg];
    UIImageView *perfilImg=[[UIImageView alloc]initWithFrame:CGRectMake(coverImg.frame.size.height*0.15, coverImg.frame.size.height*0.15, coverImg.frame.size.height*0.7, coverImg.frame.size.height*0.7)];
    perfilImg.image=[UIImage imageNamed:@"perfil"];
    perfilImg.layer.cornerRadius=perfilImg.frame.size.width/2;
    perfilImg.layer.masksToBounds=YES;
    [contentV addSubview:perfilImg];
    //UIButton *perfilBtn=[UIButton alloc]ini
    
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
