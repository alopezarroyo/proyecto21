//
//  GeoLocVC.m
//  MSD
//
//  Created by Lightsoft on 16/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "GeoLocVC.h"
#import "UIViewController+LMSideBarController.h"

@interface GeoLocVC ()

@end

@implementation GeoLocVC

@synthesize mapView;
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}

@synthesize titleStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    
    /*
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    */
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;

    self.mapView.hidden = true;
    self.textFieldSearch.delegate = self;
    
    [self.textFieldSearch addTarget:self action:@selector(textFieldDidReturn:) forControlEvents:UIControlEventEditingDidEndOnExit];
    printf("viewDidLoad");

    if ([CLLocationManager locationServicesEnabled] )
    {
        printf("locationServicesEnabled");
        if (self.locationManager == nil )
        {
            printf("init location manager");

            self.locationManager = [[CLLocationManager alloc] init];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
            self.locationManager.distanceFilter = kCLDistanceFilterNone; //kCLDistanceFilterNone// kDistanceFilter;
            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
            [self.locationManager requestWhenInUseAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
    


    //CLLocationCoordinate2D  ctrpoint;
    //  ctrpoint.latitude = ;
    //ctrpoint.longitude =f1;
    //coordinate.latitude=23.6999;
    //coordinate.longitude=75.000;

   
   
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.title=self.titleStr;
    
    NSLog(@"titleView%@",self.titleStr);
    self.mapView.hidden = true;
    
    
    
    CLLocationCoordinate2D coordinate;
    
    coordinate.latitude= self.locationManager.location.coordinate.latitude;
    coordinate.longitude= self.locationManager.location.coordinate.longitude;
    MKPointAnnotation *marker = [MKPointAnnotation new];
    marker.coordinate = coordinate;
    marker.title = @"Location";
    NSLog(@"%f",coordinate.latitude);
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.mapView addAnnotation:marker];
    });
    self.descriptionArray = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22"];

    NSArray *name=[[NSArray alloc]initWithObjects:
                   @"Sanborn's Aeropuerto",
                   @"Sanborn's Pabellón Altavista",
                   @"Sanborn's Antonio Caso",
                   @"Sanborn's Aviamex",
                   @"Sanborn's Azcapotzalco",
                   @"Sanborn's Balbuena",
                   @"Sanborn's Barranca del Muerto",
                   @"Sanborn's Boker",
                   @"Sanborn's Buenavista",
                   @"Sanborn's Camarones",
                   @"Sanborn's Centenario",
                   @"Sanborn's Centro Histórico TACUBA",
                   @"Sanborn's Centro Insurgentes",
                   @"Sanborn's Coacalco Zentralia",
                   @"Sanborn's Coacalco Power Center",
                   @"Sanborn's Cuauhtemoc",
                   @"Sanborn's Cuautitlan",
                   @"Sanborn's Cuautitlan San Marcos",
                   @"Sanborn's Cuicuilco Peña Pobre",
                   @"Sanborn's Cuautitlan Luna Parc",
                   @"Sanborn's Churubusco",
                   @"Sanborn's Ciudad Jardín Nezahualcoyot",
                   @"Sanborn's Del Valle",
                   
                   
                   nil];
    
    NSMutableArray *annotation=[[NSMutableArray alloc]initWithCapacity:[name count]];

    
    MKPointAnnotation *mappin;
    
    CLLocationCoordinate2D location;
    
    location = CLLocationCoordinate2DMake(19.4238691,-99.0950103);
    mappin = [[MKPointAnnotation alloc]init];
    mappin.coordinate=location;
    mappin.title= [name objectAtIndex:0];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3296095,-99.2893164);
    mappin.coordinate=location;
    mappin.title= [name objectAtIndex:1];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4342056,-99.1589615);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:2];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3957417,-99.1685707);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:3];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4777991,-99.1888121);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:4];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4201113,-99.1162753);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:5];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3639725,-99.1942085);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:6];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4326813,-99.1386353);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:7];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.442136,-99.1570107);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:8];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4605256,-99.1727464);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:9];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3495627,-99.1655446);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:10];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4368146,-99.1423841);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:11];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];

    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3635068,-99.1847309);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:12];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.6320251,-99.0908244);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:13];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.6284307,-99.1260633);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:14];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.422887,-99.1617648);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:15];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.6494629,-99.199761);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:16];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.6708675,-99.2036844);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:17];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.301009,-99.1875907);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:18];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.6572918,-99.2127867);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:19];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3591905,-99.1721528);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:20];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.4243649,-99.0252795);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:21];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    mappin = [[MKPointAnnotation alloc]init];
    location = CLLocationCoordinate2DMake(19.3748596,-99.1804257);
    mappin.coordinate=location;
    mappin.title=[name objectAtIndex:22];
    mappin.subtitle = @"Detail";
    [annotation addObject:mappin];
    
    [self.mapView addAnnotations:annotation];
    
    [self.locationManager startUpdatingLocation];
    
    mapView.showsUserLocation = YES;
    [mapView setMapType:MKMapTypeStandard];
    [mapView setZoomEnabled:YES];
    [mapView setScrollEnabled:YES];
    
    self.locationManager.distanceFilter = kCLDistanceFilterNone;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    [self.locationManager startUpdatingLocation];
   

/*    NSMutableArray *arrayOfPoints = [[NSMutableArray alloc] init];
    
    [arrayOfPoints addObject:[NSValue valueWithCGPoint:CGPointMake(19.4238691, -99.0950103)]];
    
    for (int i = 0; i <= arrayOfPoints.count; i++)
    {
        if (strcmp([arrayOfPoints[i] objCType], @encode(CGPoint)) == 0) {
            CGPoint point = [arrayOfPoints[i] CGPointValue];
            MKPointAnnotation *marker = [MKPointAnnotation new];
            CLLocationCoordinate2D coordinate;
            
            coordinate.latitude= point.x;
            coordinate.longitude= point.y;

            marker.coordinate = coordinate;
            marker.title = @"Locationz";
            NSLog(@"%f",coordinate.latitude);
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.mapView addAnnotation:marker];
            });

        }
        

    }*/
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    
    return YES;
}

// It is important for you to hide the keyboard
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidReturn:(UITextField *)textField
{
    // Execute additional code
    self.mapView.hidden = false;
    
    [self.view endEditing:YES];
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.mapView.hidden = false;
    
    [self.view endEditing:YES];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    printf("did update locations ");

    self.currentLocation = [locations lastObject];
    // here we get the current location
}

- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    
    /*
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(userLocation.coordinate, 19.7903475,-98.3889916);
    [self.mapView setRegion:[self.mapView regionThatFits:region] animated:YES];
    
    // Add an annotation
    MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
    point.coordinate = userLocation.coordinate;
    point.title = @"Test";
    point.subtitle = @"It's here!!!";
    
    [self.mapView addAnnotation:point];
     
     */
    printf("did update user location ");

    if (!self.initialLocation) {
        printf("did update user location enter");
        self.initialLocation = userLocation.location;
        MKCoordinateRegion mapRegion;
        mapRegion.center = self.mapView.userLocation.coordinate;
        mapRegion.span.latitudeDelta = 0.2;
        mapRegion.span.longitudeDelta = 0.2;
        
        [self.mapView setRegion:mapRegion animated: YES];
    }

    
    

}

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation
{
    printf("pin pressed");
    
   if ([[annotation title] isEqualToString:@"Current Location"]) {
        return nil;
    }
    
    MKAnnotationView *annView = [[MKAnnotationView alloc ] initWithAnnotation:annotation reuseIdentifier:@"currentloc"];
    if ([[annotation title] isEqualToString:@"Test"])
        annView.image = [ UIImage imageNamed:@"icono_geoloc.png" ];
    else
        annView.image = [ UIImage imageNamed:@"icono_geoloc.png" ];
    
    
    UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
    [infoButton addTarget:self action:@selector(showDetailsView)
         forControlEvents:UIControlEventTouchUpInside];
    annView.rightCalloutAccessoryView = infoButton;
    annView.canShowCallout = YES;
    return annView;
}

-(void)showDetailsView{
    
    
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    printf("annotation pressed");
   // self.detailView.hidden = NO;
    self.textDetail.text = view.annotation.title;


}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self.locationManager startUpdatingLocation];
        [self performSegueWithIdentifier:@"startSegue" sender:self];
    } else if (status == kCLAuthorizationStatusDenied) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location services not authorized"
                                                        message:@"This app needs you to authorize locations services to work."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else
        NSLog(@"Wrong location status");
}




- (IBAction)onSearchPressed:(id)sender {
    
    self.mapView.hidden = false;
    
    [self.view endEditing:YES];
}
@end
