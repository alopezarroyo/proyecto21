//
//  RecetaViewController.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 26/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecetaViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *labelTitulo;
@property (weak, nonatomic) IBOutlet UIImageView *imgReceta;
@property (weak, nonatomic) IBOutlet UITextView *textReceta;

@property NSInteger * first;
@property NSInteger *second;

@end
