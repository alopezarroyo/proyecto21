//
//  MainNC.h
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MenuVC.h"
#import "LMOthersViewController.h"
#import "GeoLocVC.h"
#import "CuentaVC.h"
#import "HistorialVC.h"
#import "CatalogoVC.h"
#import "PrefVC.h"
#import "InscribeVC.h"
#import "RegisterVC.h"
#import "EjerciciosViewController.h"

@interface MainNC : UINavigationController

@property (nonatomic, strong) MenuVC *homeViewController;
@property (nonatomic, strong) LMOthersViewController *othersViewController;
@property (nonatomic, strong) GeoLocVC *geoViewController;
@property (nonatomic, strong) CuentaVC *cuentaViewController;
@property (nonatomic, strong) HistorialVC *historialViewController;
@property (nonatomic, strong) CatalogoVC *catalogoViewController;
@property (nonatomic, strong) PrefVC *prefViewController;
@property (nonatomic, strong) InscribeVC *inscribeVCController;
@property (nonatomic, strong) RegisterVC *registerVCController;
@property (nonatomic, strong) EjerciciosViewController *ejerciciosVCController;

- (void)showHomeViewController;

- (void)showOthersViewController;

- (void)showGeoViewController;

- (void)showCuentaViewController;

- (void)showHistorialViewController;

- (void)showCatalogoViewController;

- (void)showPrefViewController;

- (void)showInscribeViewController;

- (void)showRegisterVCController;

-(void)showEjerciciosViewController;

@end
