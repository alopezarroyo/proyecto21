//
//  MainNC.m
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "MainNC.h"

@interface MainNC ()

@end

@implementation MainNC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (MenuVC *)homeViewController
{
    if (!_homeViewController) {
        _homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"homeViewController"];
    }
    return _homeViewController;
}

- (LMOthersViewController *)othersViewController
{
    if (!_othersViewController) {
        _othersViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"othersViewController"];
    }
    return _othersViewController;
}


- (GeoLocVC *)geoViewController
{
    if (!_geoViewController) {
        _geoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"geoViewController"];
    }
    return _geoViewController;
}

- (CuentaVC *)cuentaViewController
{
    if (!_cuentaViewController) {
        _cuentaViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"cuentaViewController"];
    }
    return _cuentaViewController;
}

- (HistorialVC *)historialViewController
{
    if (!_historialViewController) {
        _historialViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"historialViewController"];
    }
    return _historialViewController;
}

- (CatalogoVC *)catalogoViewController
{
    if (!_catalogoViewController) {
        _catalogoViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"catalogoViewController"];
    }
    return _catalogoViewController;
}

- (PrefVC *)prefViewController
{
    if (!_prefViewController) {
        _prefViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"prefViewController"];
    }
    return _prefViewController;
}


- (InscribeVC *)inscribeVCController
{
    if (!_inscribeVCController) {
        _inscribeVCController = [self.storyboard instantiateViewControllerWithIdentifier:@"inscribeVCController"];
    }
    return _inscribeVCController;
}

- (RegisterVC *)registerVCController
{
    if (!_registerVCController) {
        _registerVCController = [self.storyboard instantiateViewControllerWithIdentifier:@"registerVCController"];
    }
    return _registerVCController;
}

- (EjerciciosViewController *)ejerciciosVCController
{
    if (!_ejerciciosVCController) {
        _ejerciciosVCController = [self.storyboard instantiateViewControllerWithIdentifier:@"ejerciciosVCController"];
    }
    return _ejerciciosVCController;
}

- (void)showHomeViewController
{
    [self setViewControllers:@[self.homeViewController] animated:YES];
}

- (void)showOthersViewController
{
    [self setViewControllers:@[self.othersViewController] animated:YES];
}

- (void)showGeoViewController
{
    [self setViewControllers:@[self.geoViewController] animated:YES];
}

- (void)showCuentaViewController
{
    [self setViewControllers:@[self.cuentaViewController] animated:YES];
}

- (void)showHistorialViewController
{
    [self setViewControllers:@[self.historialViewController] animated:YES];
}

- (void)showCatalogoViewController
{
    [self setViewControllers:@[self.catalogoViewController] animated:YES];
}

- (void)showPrefViewController
{
    [self setViewControllers:@[self.prefViewController] animated:YES];
}

- (void)showInscribeViewController
{
    [self setViewControllers:@[self.inscribeVCController] animated:YES];
}

- (void)showRegisterVCController
{
    [self setViewControllers:@[self.registerVCController] animated:YES];
}

- (void)showEjerciciosViewController
{
    [self setViewControllers:@[self.ejerciciosVCController] animated:YES];
}
@end
