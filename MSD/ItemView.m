//
//  ItemView.m
//  MSD
//
//  Created by Rene Cabañas Lopez on 08/01/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "ItemView.h"

@implementation ItemView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithFrame:(CGRect)frame
{
    NSLog(@"%s", __FUNCTION__);
    
    self = [super initWithFrame:frame];
    if (self)
    {
        NSArray *nibContents =
        [[NSBundle mainBundle] loadNibNamed:@"ItemView"
                                      owner:self
                                    options:nil];
        [self addSubview:nibContents[0]];
    }
    return self;
}

@end
