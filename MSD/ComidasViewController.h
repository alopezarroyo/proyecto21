//
//  ComidasViewController.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ComidasViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>
@property (strong, nonatomic) NSArray * comidas;
@property (strong, nonatomic) NSArray * imagenComidas;


@end
