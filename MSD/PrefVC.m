//
//  PrefVC.m
//  MSD
//
//  Created by Rene Cabañas Lopez on 11/01/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "PrefVC.h"
#import "UIViewController+LMSideBarController.h"
#import "ItemPrefCell.h"

@interface PrefVC ()

@end

@implementation PrefVC

@synthesize titleStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    
    /*
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    */
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.title=self.titleStr;
    
    NSLog(@"titleView%@",self.titleStr);
    
}

- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ItemPrefCell *cell = [tableView dequeueReusableCellWithIdentifier:@"itemPrefCell"];
    
    if(indexPath.row==0){
        cell.labelText.text = @"¿Desea que la aplicación conozca su ubicación?";
    }//else if(indexPath.row==1){
       // cell.labelText.text = @"¿Le gustaría inscribirse al programa?";
    //}else if(indexPath.row==2){
        //cell.labelText.text = @"¿Acepta los Términos y Condiciones del Programa? Le informamos que se encuntran disponibles aquí.";
    //}else if(indexPath.row==3){
      //  cell.labelText.text = @"¿Otorga usted el consentimiento del Aviso de Privacidad y acepta recibir bonificaciones?";
    //}else if(indexPath.row==4){
        //cell.labelText.text = @"¿Acepta usted ser el contacto para recibir información adicional de su programa y otros beneficios?";
    //}





    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

@end
