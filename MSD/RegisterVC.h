//
//  RegisterVC.h
//  MSD
//
//  Created by Rene Cabañas Lopez on 06/02/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController {
    
    NSString *titleStr;
    
}

@property (strong, nonatomic) NSArray * input;
@property(strong,nonatomic) NSString *titleStr;

@property(nonatomic,strong)NSMutableData *webData;

@property(nonatomic,strong)IBOutlet UIView *form1;
@property(nonatomic,strong)IBOutlet UIView *form2;
@property(nonatomic,strong)IBOutlet UIView *form3;
@property(nonatomic,strong)IBOutlet UIView *form4;
@property(nonatomic,strong)IBOutlet UIView *form5;
@property(nonatomic,strong)IBOutlet UIView *form6;
@property(nonatomic,strong)IBOutlet UIView *form7;
@property(nonatomic,strong)IBOutlet UIView *termsConditions;

@property(nonatomic,strong)IBOutlet UILabel *labelTitleF1;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF2A;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF2B;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF3;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF4;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF6A;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF6B;

@property(nonatomic,strong)IBOutlet UIButton *btnContinuar;

//Botones Formulario 1
@property(nonatomic,strong)IBOutlet UIView *contentBtn1;
@property(nonatomic,strong)IBOutlet UIButton *btnF1A;

@property(nonatomic,strong)IBOutlet UIView *contentBtn2;
@property(nonatomic,strong)IBOutlet UIButton *btnF1B;

@property(nonatomic,strong)IBOutlet UIView *contentBtn3;
@property(nonatomic,strong)IBOutlet UIButton *btnF1C;

@property(nonatomic,strong)IBOutlet UIView *contentBtn4;
@property(nonatomic,strong)IBOutlet UIButton *btnF1D;


//Botones Formulario 2
@property(nonatomic,strong)IBOutlet UIView *contentBtn1F2;
@property(nonatomic,strong)IBOutlet UIButton *btnF2A;

@property(nonatomic,strong)IBOutlet UIView *contentBtn2F2;
@property(nonatomic,strong)IBOutlet UIButton *btnF2B;

@property(nonatomic,strong)IBOutlet UIView *contentBtn3F2;
@property(nonatomic,strong)IBOutlet UIButton *btnF2C;

@property(nonatomic,strong)IBOutlet UIView *contentBtn4F2;
@property(nonatomic,strong)IBOutlet UIButton *btnF2D;

//Botones Formulario 3
@property(nonatomic,strong)IBOutlet UIView *contentBtn1F3;
@property(nonatomic,strong)IBOutlet UIButton *btnF3A;

@property(nonatomic,strong)IBOutlet UIView *contentBtn2F3;
@property(nonatomic,strong)IBOutlet UIButton *btnF3B;

//Botones Formulario 4
@property(nonatomic,strong)IBOutlet UIView *contentBtn1F4;
@property(nonatomic,strong)IBOutlet UIButton *btnF4A;

@property(nonatomic,strong)IBOutlet UIView *contentBtn2F4;
@property(nonatomic,strong)IBOutlet UIButton *btnF4B;

@property(nonatomic,strong)IBOutlet UIView *contentBtn3F4;
@property(nonatomic,strong)IBOutlet UIButton *btnF4C;

@property(nonatomic,strong)IBOutlet UITextField *textF41A;
@property(nonatomic,strong)IBOutlet UITextField *textF41B;
@property(nonatomic,strong)IBOutlet UITextField *textF42A;
@property(nonatomic,strong)IBOutlet UITextField *textF42B;
@property(nonatomic,strong)IBOutlet UITextField *textF43A;
@property(nonatomic,strong)IBOutlet UITextField *textF43B;

//Botones Formulario 5
@property(nonatomic,strong)IBOutlet UIView *contenF5;

@property(nonatomic,strong)IBOutlet UITextField *textNameF3;
@property(nonatomic,strong)IBOutlet UITextField *textLastNameF3P;
@property(nonatomic,strong)IBOutlet UITextField *textLastNameF3M;

@property(nonatomic,strong)IBOutlet UILabel *labelNameF3;
@property(nonatomic,strong)IBOutlet UILabel *labelLastNameF3P;
@property(nonatomic,strong)IBOutlet UILabel *labelLastNameF3M;

//Botones Formulario 6
@property(nonatomic,strong)IBOutlet UIView *contenF6A;
@property(nonatomic,strong)IBOutlet UIView *contenF6B;
@property(nonatomic,strong)IBOutlet UIView *contenF6;

@property(nonatomic,strong)IBOutlet UIButton *btnF6A;
@property(nonatomic,strong)IBOutlet UIButton *btnF6B;

//Botones Formulario 7
@property(nonatomic,strong)IBOutlet UIView *contenF7;
@property(nonatomic,strong)IBOutlet UIView *contenF7A;

@property(nonatomic,strong)IBOutlet UILabel *labelTitleF7A;
@property(nonatomic,strong)IBOutlet UILabel *labelTitleF7B;

@property(nonatomic,strong)IBOutlet UILabel *labelNameF7;
@property(nonatomic,strong)IBOutlet UILabel *labelLastNameF7P;
@property(nonatomic,strong)IBOutlet UILabel *labelLastNameF7M;
@property(nonatomic,strong)IBOutlet UILabel *labelProfessionF7;

@property(nonatomic,strong)IBOutlet UITextField *textNameF7;
@property(nonatomic,strong)IBOutlet UITextField *textLastNameF7P;
@property(nonatomic,strong)IBOutlet UITextField *textLastNameF7M;

@end
