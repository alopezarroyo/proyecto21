//
//  BaseVC.h
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseVC : UIViewController
{
    UIImageView *logoImg;
    BOOL isAppeared;
    UIButton *menuBtn;
    UIView *fondoBlanco;
    UINavigationBar *navBar;
}

@end
