//
//  ContenidoCell.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 25/04/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContenidoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitulo;
@property (weak, nonatomic) IBOutlet UILabel *lblDescripcion;
@property (weak, nonatomic) IBOutlet UIImageView *imgDesayunos;

@end
