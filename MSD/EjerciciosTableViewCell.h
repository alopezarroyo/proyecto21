//
//  EjerciciosTableViewCell.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 16/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EjerciciosTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *ejercicioImage;
@property (weak, nonatomic) IBOutlet UILabel *titleEjercicio;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;


@end
