//
//  RegisterVC.m
//  MSD
//
//  Created by Rene Cabañas Lopez on 06/02/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "RegisterVC.h"
#import "UIViewController+LMSideBarController.h"
#import "MainNC.h"
#import "JSONHelper.h"
#import "AFNetworking.h"
#import <SOAPEngine64/SOAPEngine.h>
#import "SoapKit.h"







#define colorUI [UIColor colorWithRed:37.0/255.0 green:188.0/255.0 blue:202.0/255.0 alpha:1.0]

@interface RegisterVC ()

@end

@implementation RegisterVC

@synthesize titleStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    
    /*
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    */
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
   
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.title=self.titleStr;
    
    NSLog(@"titleView%@",self.titleStr);
    
    [self.form1 setHidden:false];
    [self.form2 setHidden:true];
    [self.form3 setHidden:true];
    [self.form4 setHidden:true];
    [self.form5 setHidden:true];
    [self.form6 setHidden:true];
    [self.form7 setHidden:true];
    [self.termsConditions setHidden:true];
    
    NSMutableAttributedString *textF1title = [[NSMutableAttributedString alloc] initWithString:@"* ¿Cuenta con alguna tarjeta o folio provisional para inscribirse al programa Proyecto 21?"];
    [textF1title addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF1 setAttributedText: textF1title];
    
    NSMutableAttributedString *textF2Atitle = [[NSMutableAttributedString alloc] initWithString:@"* ¿Le gustaría inscribirse al programa?"];
    [textF2Atitle addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF2A setAttributedText: textF2Atitle];
    
    
    NSMutableAttributedString *textF2Btitle = [[NSMutableAttributedString alloc] initWithString:@"* ¿Acepta los Terminos y Condiciones del Programa? Le informamos que se encuntran disponibles aquí."];
    [textF2Btitle addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF2B setAttributedText: textF2Btitle];
    
    NSMutableAttributedString *textF3title = [[NSMutableAttributedString alloc] initWithString:@"* ¿Acepta usted ser contactado para recibir información adicional de su programa, otros beneficios e información de su padecimiento y seguimiento a su tratamiendo?"];
    [textF3title addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF3 setAttributedText: textF3title];
    
    
    NSMutableAttributedString *textF4title = [[NSMutableAttributedString alloc] initWithString:@"* ¿Por que medio(s) de contacto le gustaría ser contactado?"];
    [textF4title addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF4 setAttributedText: textF4title];
    
    
    NSMutableAttributedString *textF5titleA = [[NSMutableAttributedString alloc] initWithString:@"* Nombre"];
    [textF5titleA addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelNameF3 setAttributedText: textF5titleA];
    
    NSMutableAttributedString *textF5titleB = [[NSMutableAttributedString alloc] initWithString:@"* Apellido Paterno"];
    [textF5titleB addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelLastNameF3P setAttributedText: textF5titleB];
    
    NSMutableAttributedString *textF5titleC = [[NSMutableAttributedString alloc] initWithString:@"* Apellido Materno"];
    [textF5titleC addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelLastNameF3M setAttributedText: textF5titleC];
    
    NSMutableAttributedString *textF6titleA = [[NSMutableAttributedString alloc] initWithString:@"* Género"];
    [textF6titleA addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF6A setAttributedText: textF6titleA];
    
    NSMutableAttributedString *textF6titleB = [[NSMutableAttributedString alloc] initWithString:@"* ¿Fecha de nacimiento? (Día/Mes/Año) o fecha del paciente que va a inscribir."];
    [textF6titleB addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelTitleF6B setAttributedText: textF6titleB];
    
    
    
    NSMutableAttributedString *textF7titleA = [[NSMutableAttributedString alloc] initWithString:@"* Nombre"];
    [textF7titleA addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelNameF7 setAttributedText: textF7titleA];
    
    NSMutableAttributedString *textF7titleB = [[NSMutableAttributedString alloc] initWithString:@"* Apellido Paterno"];
    [textF7titleB addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelLastNameF7P setAttributedText: textF7titleB];
    
    NSMutableAttributedString *textF7titleC = [[NSMutableAttributedString alloc] initWithString:@"* Apellido Materno"];
    [textF7titleC addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelLastNameF7M setAttributedText: textF7titleC];
    
    NSMutableAttributedString *textF7titleD = [[NSMutableAttributedString alloc] initWithString:@"* Número de Cédula Profesional"];
    [textF7titleD addAttribute: NSForegroundColorAttributeName value: [UIColor redColor] range: NSMakeRange(0,2)];
    [self.labelProfessionF7 setAttributedText: textF7titleD];
    
    NSString * htmlString = @"<strong><font color=#25BCCA>Médico Tratante</font></strong> <small><font color=gray>(escribir nombre del médico iniciando por apellidos)</font></small>";
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    self.labelTitleF7A.attributedText = attrStr;
    
    NSString *strUnder = @"Si no aparece su médico hacer click aquí";
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strUnder];
    
    [attributedString addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(0, strUnder.length)];
    
    [self.labelTitleF7B setAttributedText: attributedString];
    
    self.contentBtn1.layer.borderColor = colorUI.CGColor;
    self.contentBtn1.layer.borderWidth = 1.0f;
    self.contentBtn2.layer.borderColor = colorUI.CGColor;
    self.contentBtn2.layer.borderWidth = 1.0f;
    self.contentBtn3.layer.borderColor = colorUI.CGColor;
    self.contentBtn3.layer.borderWidth = 1.0f;
    self.contentBtn4.layer.borderColor = colorUI.CGColor;
    self.contentBtn4.layer.borderWidth = 1.0f;
    self.contentBtn1F2.layer.borderColor = colorUI.CGColor;
    self.contentBtn1F2.layer.borderWidth = 1.0f;
    self.contentBtn2F2.layer.borderColor = colorUI.CGColor;
    self.contentBtn2F2.layer.borderWidth = 1.0f;
    self.contentBtn3F2.layer.borderColor = colorUI.CGColor;
    self.contentBtn3F2.layer.borderWidth = 1.0f;
    self.contentBtn4F2.layer.borderColor = colorUI.CGColor;
    self.contentBtn4F2.layer.borderWidth = 1.0f;
    self.contentBtn1F3.layer.borderColor = colorUI.CGColor;
    self.contentBtn1F3.layer.borderWidth = 1.0f;
    self.contentBtn2F3.layer.borderColor = colorUI.CGColor;
    self.contentBtn2F3.layer.borderWidth = 1.0f;
    self.contentBtn1F4.layer.borderColor = colorUI.CGColor;
    self.contentBtn1F4.layer.borderWidth = 1.0f;
    self.contentBtn2F4.layer.borderColor = colorUI.CGColor;
    self.contentBtn2F4.layer.borderWidth = 1.0f;
    self.contentBtn3F4.layer.borderColor = colorUI.CGColor;
    self.contentBtn3F4.layer.borderWidth = 1.0f;
    self.contenF5.layer.borderColor = colorUI.CGColor;
    self.contenF5.layer.borderWidth = 1.0f;
    self.contenF6.layer.borderColor = colorUI.CGColor;
    self.contenF6.layer.borderWidth = 1.0f;
    self.contenF6A.layer.borderColor = colorUI.CGColor;
    self.contenF6A.layer.borderWidth = 1.0f;
    self.contenF6B.layer.borderColor = colorUI.CGColor;
    self.contenF6B.layer.borderWidth = 1.0f;
    self.contenF7A.layer.borderColor = colorUI.CGColor;
    self.contenF7A.layer.borderWidth = 1.0f;
    self.contenF7.layer.borderColor = colorUI.CGColor;
    self.contenF7.layer.borderWidth = 1.0f;
    
    
    [self.btnF1A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF1B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    [self.btnF2A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF2B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    [self.btnF2C setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF2D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    [self.btnF3A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF3B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    [self.btnF4A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF4B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF4C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    [self.btnF6A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF6B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    
    
    CALayer *bottomBorderF4A = [CALayer layer];
    bottomBorderF4A.frame = CGRectMake(0.0f, self.textF41A.frame.size.height - 1, self.textF41A.frame.size.width, 1.0f);
    bottomBorderF4A.backgroundColor = colorUI.CGColor;
    [self.textF41A.layer addSublayer:bottomBorderF4A];
    
    CALayer *bottomBorderF4B = [CALayer layer];
    bottomBorderF4B.frame = CGRectMake(0.0f, self.textF41B.frame.size.height - 1, self.textF41B.frame.size.width, 1.0f);
    bottomBorderF4B.backgroundColor = colorUI.CGColor;
    [self.textF41B.layer addSublayer:bottomBorderF4B];
    
    CALayer *bottomBorderF4C = [CALayer layer];
    bottomBorderF4C.frame = CGRectMake(0.0f, self.textF42A.frame.size.height - 1, self.textF42A.frame.size.width, 1.0f);
    bottomBorderF4C.backgroundColor = colorUI.CGColor;
    [self.textF42A.layer addSublayer:bottomBorderF4C];
    
    CALayer *bottomBorderF4D = [CALayer layer];
    bottomBorderF4D.frame = CGRectMake(0.0f, self.textF42B.frame.size.height - 1, self.textF42B.frame.size.width, 1.0f);
    bottomBorderF4D.backgroundColor = colorUI.CGColor;
    [self.textF42B.layer addSublayer:bottomBorderF4D];
    
    CALayer *bottomBorderF4E = [CALayer layer];
    bottomBorderF4E.frame = CGRectMake(0.0f, self.textF43A.frame.size.height - 1, self.textF43A.frame.size.width, 1.0f);
    bottomBorderF4E.backgroundColor = colorUI.CGColor;
    [self.textF43A.layer addSublayer:bottomBorderF4E];
    
    CALayer *bottomBorderF5A = [CALayer layer];
    bottomBorderF5A.frame = CGRectMake(0.0f, self.textNameF3.frame.size.height - 1, self.textNameF3.frame.size.width, 1.0f);
    bottomBorderF5A.backgroundColor = colorUI.CGColor;
    [self.textNameF3.layer addSublayer:bottomBorderF5A];
    
    CALayer *bottomBorderF5B = [CALayer layer];
    bottomBorderF5B.frame = CGRectMake(0.0f, self.textLastNameF3P.frame.size.height - 1, self.textLastNameF3P.frame.size.width, 1.0f);
    bottomBorderF5B.backgroundColor = colorUI.CGColor;
    [self.textLastNameF3P.layer addSublayer:bottomBorderF5B];
    
    CALayer *bottomBorderF5C = [CALayer layer];
    bottomBorderF5C.frame = CGRectMake(0.0f, self.textLastNameF3M.frame.size.height - 1, self.textLastNameF3M.frame.size.width, 1.0f);
    bottomBorderF5C.backgroundColor = colorUI.CGColor;
    [self.textLastNameF3M.layer addSublayer:bottomBorderF5C];
    
    
    CALayer *bottomBorderF7A = [CALayer layer];
    bottomBorderF7A.frame = CGRectMake(0.0f, self.textNameF7.frame.size.height - 1, self.textNameF7.frame.size.width, 1.0f);
    bottomBorderF7A.backgroundColor = colorUI.CGColor;
    [self.textNameF7.layer addSublayer:bottomBorderF7A];
    
    CALayer *bottomBorderF7B = [CALayer layer];
    bottomBorderF7B.frame = CGRectMake(0.0f, self.textLastNameF7P.frame.size.height - 1, self.textLastNameF7P.frame.size.width, 1.0f);
    bottomBorderF7B.backgroundColor = colorUI.CGColor;
    [self.textLastNameF7P.layer addSublayer:bottomBorderF7B];
    
    CALayer *bottomBorderF7C = [CALayer layer];
    bottomBorderF7C.frame = CGRectMake(0.0f, self.textLastNameF7M.frame.size.height - 1, self.textLastNameF7M.frame.size.width, 1.0f);
    bottomBorderF7C.backgroundColor = colorUI.CGColor;
    [self.textLastNameF7M.layer addSublayer:bottomBorderF7C];
    
}

- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}

-(IBAction)next:(id)sender{
    
    if(!self.form1.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:false];
    }else if(!self.form2.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:false];
    }else if(!self.form3.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:true];
        [self.form4 setHidden:false];
    }else if(!self.form4.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:true];
        [self.form4 setHidden:true];
        [self.form5 setHidden:false];
    }else if(!self.form5.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:true];
        [self.form4 setHidden:true];
        [self.form5 setHidden:true];
        [self.form6 setHidden:false];
    }else if(!self.form6.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:true];
        [self.form4 setHidden:true];
        [self.form5 setHidden:true];
        [self.form6 setHidden:true];
        [self.form7 setHidden:false];

    }else if(!self.form7.isHidden){
        [self.form1 setHidden:true];
        [self.form2 setHidden:true];
        [self.form3 setHidden:true];
        [self.form4 setHidden:true];
        [self.form5 setHidden:true];
        [self.form6 setHidden:true];
        [self.form7 setHidden:true];
        [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.termsConditions.frame  = CGRectMake(0, self.termsConditions.frame.origin.y, self.termsConditions.frame.size.width,self.termsConditions.frame.size.height);
            [self.termsConditions setHidden:false];

        } completion:^(BOOL finished) {
            
        }];
    }
}

-(IBAction)showMain:(id)sender{
     //[self newService];
    //[self testService];
    [self testService2];
    //[self testService3];
    //[self webService];
    //[self testNew];
    //[self soapService];
    //[self soapServiceNew];
    MainNC *mainNavigationController = (MainNC *)self.sideBarController.contentViewController;
    [mainNavigationController showHomeViewController];
    [self.sideBarController hideMenuViewController:YES];
}


-(IBAction)buttonSelectF1A:(id)sender {
    [self.btnF1A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF1B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF1B:(id)sender {
    [self.btnF1A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1B setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF1C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF1C:(id)sender {
    [self.btnF1A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1C setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF1D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF1D:(id)sender {
    [self.btnF1A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF1D setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF2A:(id)sender {
    [self.btnF2A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF2B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF2B:(id)sender {
    [self.btnF2A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF2B setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF2C:(id)sender {
    [self.btnF2C setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF2D setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF2D:(id)sender {
    [self.btnF2C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF2D setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}


-(IBAction)buttonSelectF3A:(id)sender {
    [self.btnF3A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF3B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF3B:(id)sender {
    [self.btnF3A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF3B setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF4A:(id)sender {
    [self.btnF4A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF4B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF4C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF4B:(id)sender {
    [self.btnF4A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF4B setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF4C setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF4C:(id)sender {
    [self.btnF4A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF4B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF4C setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}


-(IBAction)buttonSelectF6A:(id)sender {
    [self.btnF6A setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
    [self.btnF6B setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
}

-(IBAction)buttonSelectF6B:(id)sender {
    [self.btnF6A setImage:[UIImage imageNamed:@"btn_white"] forState:UIControlStateNormal];
    [self.btnF6B setImage:[UIImage imageNamed:@"btn_select"] forState:UIControlStateNormal];
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */












#pragma mark - SOAPEngine Delegates

- (void)soapEngine:(SOAPEngine *)soapEngine didFinishLoading:(NSString *)stringXML {
    
    NSDictionary *result = [soapEngine dictionaryValue];
    // read data from a dataset table
    NSArray *list = [result valueForKeyPath:@"NewDataSet.Table"];
}



- (void) soapServiceNew{
    
    SOAPEngine *soap = [[SOAPEngine alloc] init];
    //soap.userAgent = @"SOAPEngine";
    soap.delegate = self; // use SOAPEngineDelegate
    NSDate *now = [NSDate date];
    NSString *str = @"10/08/1976"; /// here this is your date with format yyyy-MM-dd
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    //str = [NSDateFormatter formatter:now];
    
    
    soap.selfSigned = YES; // only for invalid https certificates
    soap.responseHeader = YES;
    soap.actionNamespaceSlash = YES;
    soap.envelope = @"xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"";
     NSLog(@"DATE: %@", str);
    
    [soap setValue:@"7603000000003" forKey:@"tarjeta"];
    [soap setValue:@"10/08/1976" forKey:@"fechanacimiento"];
    [soap setValue:@"1" forKey:@"idmovil"];
    [soap setValue:@"1" forKey:@"tipomovil"];
    [soap setValue:@"1d93235283cb3a61d04b7e6ce7d3b97f" forKey:@"key"];

     NSLog(@"SOAP: %@", soap.description);
    
    [soap requestURL:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx"
          soapAction:@"http://tempuri.org/ValidaTarjeta"
completeWithDictionary:^(NSInteger statusCode, NSDictionary *dict) {
     NSLog(@"Result: %ld", (long)statusCode);
    NSLog(@"Result: %@", dict);
    
} failWithError:^(NSError *error) {
    
    NSLog(@"%@", error);
}];
    
  
  

    
//    SOAPEngine *soap = [[SOAPEngine alloc] init];
//    soap.userAgent = @"SOAPEngine";
//    soap.delegate = self; // use SOAPEngineDelegate
//    
//    // each single value
//    [soap setValue:@"my-value1" forKey:@"Param1"];
//    [soap setIntegerValue:1234 forKey:@"Param2"];
//    // service url without ?WSDL, and you can search the soapAction in the WSDL
    
}













- (void) soapService{
    NSString *soapmessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<tem:ValidaTarjeta>\n"
                             "<tem:tarjeta>%@</tem:tarjeta>\n"
                             "<tem:fechanacimiento>%@</tem:fechanacimiento>\n"
                             "<tem:idmovil>%@</tem:idmovil>\n"
                             "<tem:tipomovil>%@</tem:tipomovil>\n"
                             "<tem:key>%@</tem:key>\n"
                             "</tem:ValidaTarjeta>\n"
                             "</soapenv:Envelope>\n"
                             ,@"7603000000001",
                             @"14/03/1968",
                             @"as@mail.com",
                             @"iOS",
                             @"1d93235283cb3a61d04b7e6ce7d3b97f"];
    
    
    NSURL *url = [NSURL URLWithString:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest requestWithURL:url];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapmessage length]];
    
    NSLog(@"DICCIONARIO %@", soapmessage);
    
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue: @"http://tempuri.org/ValidaTarjeta" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    
 
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapmessage dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if(theConnection ){
        NSLog(@"theConnection %@", theConnection);
        _webData = [NSMutableData data];
         NSLog(@"WEBDATA %@", _webData);
    }
        //
    else
        NSLog(@"theConnection is NULL");
    
    
}








- (void) testNew{
//    NSURL *URL = [NSURL URLWithString:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx"];
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    [manager GET:URL.absoluteString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        NSLog(@"Error: %@", error);
//    }];
    NSString *soapmessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<tem:ValidaTarjeta>\n"
                             "<tem:tarjeta>%@</tem:tarjeta>\n"
                             "<tem:fechanacimiento>%@</tem:fechanacimiento>\n"
                             "<tem:idmovil>%@</tem:idmovil>\n"
                             "<tem:tipomovil>%@</tem:tipomovil>\n"
                             "<tem:key>%@</tem:key>\n"
                             "</tem:ValidaTarjeta>\n"
                             "</soapenv:Envelope>\n"
                             ,@"7603000000001",
                             @"14/03/1968",
                             @"as@mail.com",
                             @"iOS",
                             @"1d93235283cb3a61d04b7e6ce7d3b97f"];
    NSAttributedString *attributedString = [[NSAttributedString alloc]
                                            initWithData: [soapmessage dataUsingEncoding:NSUnicodeStringEncoding]
                                            options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                            documentAttributes: nil
                                            error: nil
                                            ];

    //NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
//    NSError *error;      // Initialize NSError
//    
//
//    NSDictionary *params = @{@"input":@{@"tarjeta":@"7603000000006",@"fecnacimiento": @"21/04/1986",@"idmovil":@"as@mail.com",@"tipoMovil":@"iOS",@"key":@"1d93235283cb3a61d04b7e6ce7d3b97f"}};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx" parameters:soapmessage progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        _webData = [NSMutableData data];
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}


- (void) testService3{
    
    NSString *strURL = @"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx";
   // NSDictionary *dictParamiters = @{@"user[height]": height,@"user[weight]": weight};
    NSDictionary *params = @{@"tarjeta":@"7603000000006",@"fecnacimiento": @"21/04/1986",@"idmovil":@"as@mail.com",@"tipoMovil":@"iOS",@"key":@"1d93235283cb3a61d04b7e6ce7d3b97f"};
    
    NSString *aStrParams = [self getFormDataStringWithDictParams:params];
    
    NSData *aData = [aStrParams dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableURLRequest *aRequest = [[NSMutableURLRequest alloc]initWithURL:[NSURL URLWithString:strURL]];
    [aRequest setHTTPMethod:@"POST"];
    [aRequest setHTTPBody:aData];
    
    NSLog(@"DICCIONARIO:%@",aStrParams);
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:nil delegateQueue:nil];
    [aRequest setHTTPBody:aData];
    
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:aRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        //
        if (error ==nil) {
            NSString *aStr = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
            NSLog(@"ResponseString:%@",aStr);
            NSMutableDictionary *aMutDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            dispatch_async(dispatch_get_main_queue(), ^{
                //completionBlock(aMutDict);
                
                NSLog(@"responce:%@",aMutDict);
                
            });
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                NSLog(@"error:%@",error);
            });
        }
    }];
    
    [postDataTask resume];
    
}


-(NSString *)getFormDataStringWithDictParams:(NSDictionary *)aDict
{
    NSMutableString *aMutStr = [[NSMutableString alloc]initWithString:@""];
    for (NSString *aKey in aDict.allKeys) {
        [aMutStr appendFormat:@"%@=%@&",aKey,aDict[aKey]];
    }
    NSString *aStrParam;
    if (aMutStr.length>2) {
        aStrParam = [aMutStr substringWithRange:NSMakeRange(0, aMutStr.length-1)];
        
    }
    else
        aStrParam = @"";
    
    return aStrParam;
}




- (void) testService2{
  
    NSString *urlStringgetairport = [NSString stringWithFormat:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx"];
    NSURL *getairportUrl = [NSURL URLWithString:urlStringgetairport];
    NSMutableURLRequest *finalRequest = [NSMutableURLRequest requestWithURL:getairportUrl];
    NSString *soapmessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<tem:ValidaTarjeta>\n"
                             "<tem:tarjeta>%@</tem:tarjeta>\n"
                             "<tem:fechanacimiento>%@</tem:fechanacimiento>\n"
                             "<tem:idmovil>%@</tem:idmovil>\n"
                             "<tem:tipomovil>%@</tem:tipomovil>\n"
                             "<tem:key>%@</tem:key>\n"
                             "</tem:ValidaTarjeta>\n"
                             "</soapenv:Envelope>\n"
                             ,@"7603000000001",
                             @"14/03/1968",
                             @"as@mail.com",
                             @"iOS",
                             @"1d93235283cb3a61d04b7e6ce7d3b97f"];
    
   // NSURL *url = [NSURL URLWithString:@"http://webapp.admin-inapp.com/services/FeedbackService.asmx?op=SendFeedback"];
    NSMutableURLRequest *theRequest = [NSMutableURLRequest     requestWithURL:getairportUrl];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapmessage length]];
    [theRequest addValue: @"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    [theRequest addValue:@"http://tempuri.org/ValidaTarjeta" forHTTPHeaderField:@"SOAPAction"];
    [theRequest addValue: msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setHTTPBody: [soapmessage dataUsingEncoding:NSUTF8StringEncoding]];
    [theRequest setTimeoutInterval:10];
    NSURLConnection *theConnection = [[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    
    NSData *soapdata = [soapmessage dataUsingEncoding:NSUTF8StringEncoding];
    NSString *messageLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapmessage length]];
    
    NSLog(@"DICCIONARIO:%@",soapmessage);
    //responseData = [[NSMutableData data] retain];
    
    NSLog(@"REQUEST:%@",finalRequest);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    //From here replace your code
    
    
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    [manager GET:urlStringgetairport parameters:soapdata progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        
        NSDictionary *dict = responseObject;
        NSLog(@"output :%@", dict);

      
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        
    }];
    
//    [manager POST:urlStringgetairport parameters:soapdata progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        
//        
//        NSLog(@"Response from server :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
//        
//        [self getdata:responseObject];
//        
//        
//    }
//          failure:^(NSURLSessionTask *operation, NSError *error)
//     
//     {
//         
//         NSLog(@"Error: %@", error);
//         
//     }];
//    
  
    
    
 
//
//    [finalRequest addValue:@"text/xml; charset=utf-8" forHTTPHeaderField:@"Content-Type"];
//    [finalRequest addValue:@"http://tempuri.org/ValidaTarjeta" forHTTPHeaderField:@"SOAPAction"];
//    [finalRequest addValue:@"www.appcacto.com" forHTTPHeaderField:@"HOST"];
//    [finalRequest addValue:messageLength forHTTPHeaderField:@"Content-Length"];
//    [finalRequest setHTTPMethod:@"POST"];
//    [finalRequest setHTTPBody:soapdata];
//
  

    
//    NSURLSessionDataTask *dataTask = [manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        
//        NSData *data = responseObject;
//        NSDictionary *dictTemp = [NSDictionary dictionaryWithXMLData:data]; //XMLDictionary will use to parse xml
//        NSLog(@"%@",dictTemp);
//        
//    }];
//    
//    [dataTask resume];
    

    
}





- (void) testService{
    NSString *Loginurl = [NSString stringWithFormat:@"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx"];
    
     NSError *error;      // Initialize NSError
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"curp"] = @"";
    
//            NSMutableDictionary *params = [NSMutableDictionary dictionary];
//            params[@"tarjeta"] = @"1234567890123";
//            params[@"fechaNacimiento"] = @"21/04/1986";
//            params[@"idmovil"] = @"as@mail.com";
//            params[@"key"] = @"1d93235283cb3a61d04b7e6ce7d3b97f";
//            params[@"tipoMovil"] = @"iOS";
    
    NSDictionary *params = @{@"input":@{@"tarjeta":@"7603000000006",@"fechaNacimiento": @"21/04/1986",@"idmovil":@"as@mail.com",@"key":@"1d93235283cb3a61d04b7e6ce7d3b97f",@"tipoMovil":@"iOS"}};
    
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:params options:0 error:&error];  // Convert your parameter to NSDATA
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];  // Convert data into string using NSUTF8StringEncoding
    
    NSLog(@"Sent parameter to server : %@",params);
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    manager.responseSerializer = [AFJSONResponseSerializer serializerWithReadingOptions:NSJSONReadingAllowFragments];
    
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    
    AFSecurityPolicy* policy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeCertificate];
    
    [policy setValidatesDomainName:NO];
    
    [policy setAllowInvalidCertificates:YES];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"application/soap+xml", nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/html",@"application/x-www-form-urlencoded",@"application/soap+xml",nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json",@"text/plain",@"application/soap+xml",nil];
    
    
    
    [manager POST:Loginurl parameters:params progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        
        
        NSLog(@"Response from server :  %@", [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
        
        [self getdata:responseObject];
        
        
    }
          failure:^(NSURLSessionTask *operation, NSError *error)
     
     {
         
         NSLog(@"Error: %@", error);
         
     }];    
}


-(void)getdata:(NSDictionary*)RegisterData
{
    
    
}





- (void) newService{
    
    //NSDictionary *dict1 = @{@"curp":@"LOAA860421HDFPRL02"};
    
    
    
   // NSString *URLString = @"https://development.esar.io/uat/sartoken/descarga/validarcurp?api_key=2hcqgk3ftxsx8w3k95fn9pdc";
    
    NSString *URLString = @"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx";    // Enter your url
    //NSDictionary *parameters =@{@"id" : @"52"};  // Add your parameters
    
    NSString *soapmessage = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\">\n"
                             "<tem:ValidaTarjeta>\n"
                             "<tem:tarjeta>%@</tem:tarjeta>\n"
                             "<tem:fechanacimiento>%@</tem:fechanacimiento>\n"
                             "<tem:idmovil>%@</tem:idmovil>\n"
                             "<tem:tipomovil>%@</tem:tipomovil>\n"
                             "<tem:key>%@</tem:key>\n"
                             "</tem:ValidaTarjeta>\n"
                             "</soapenv:Envelope>\n"
                             ,@"7603000000001",
                             @"14/03/1968",
                             @"as@mail.com",
                             @"iOS",
                             @"1d93235283cb3a61d04b7e6ce7d3b97f"];

    
    NSDictionary *dict = @{@"input":@{@"tarjeta":@"1234567890123",@"fechaNacimiento": @"21/04/1986",@"idmovil":@"as@mail.com",@"key":@"1d93235283cb3a61d04b7e6ce7d3b97f",@"tipoMovil":@"iOS"}};
    NSError *error;      // Initialize NSError
    
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:soapmessage options:0 error:&error];  // Convert your parameter to NSDATA
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];  // Convert data into string using NSUTF8StringEncoding
    
     NSLog(@"DICCIONARIO: %@", jsonString);
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]]; //Intialialize AFURLSessionManager
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:URLString parameters:nil error:nil];  // make NSMutableURL req
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue]; // add paramerets to NSMutableURLRequest
    [req setValue:@"/MobileAppLiomont/appLiomont.asmx/ValidaTarjeta" forHTTPHeaderField:@"Content-Type"];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            
            if ([responseObject isKindOfClass:[NSArray class]]) {
                
                NSLog(@"Response == %@",responseObject);
                
            }
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
        }
        
    }]resume];
    
    
}







- (void) servicioP21{
    NSString *url = @"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx";
    // NSString *url = @"https://development.esar.io/uat/sartoken/descarga/validarcurp?api_key=2hcqgk3ftxsx8w3k95fn9pdc";
     //NSDictionary *dict1 = @{@"curp":@"LOAA860421HDFPRL02"};
   // /MobileAppLiomont/appLiomont.asmx/ValidaTarjeta/MobileAppLiomont/appLiomont.asmx/ValidaTarjeta
    
    //NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
   //                         @"LOAA860421HDFPRL02",@"curp"];
 
//        NSMutableDictionary *params = [NSMutableDictionary dictionary];
//        params[@"tarjeta"] = @"1234567890123";
//        params[@"fechaNacimiento"] = @"21/04/1986";
//        params[@"idmovil"] = @"as@mail.com";
//        params[@"key"] = @"1d93235283cb3a61d04b7e6ce7d3b97f";
//        params[@"tipoMovil"] = @"iOS";
    
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"curp"] = @"LOAA860421HDFPRL02";
   
    NSDictionary *dict = @{@"input":@{@"tarjeta":@"1234567890123",@"fechaNacimiento": @"21/04/1986",@"idmovil":@"as@mail.com",@"key":@"1d93235283cb3a61d04b7e6ce7d3b97f",@"tipoMovil":@"iOS"}};
    
    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
//                                                       options:0
//                                                         error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict options:0 error:&error];  // Convert your parameter to NSDATA
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];  // Convert data into string using NSUTF8StringEncoding
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"/MobileAppLiomont/appLiomont.asmx/ValidaTarjeta" forHTTPHeaderField:@"Content-Type"];
    

    
//    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc]initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    manager.requestSerializer = [AFJSONRequestSerializer serializer];
//    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"text/xml"];
    
    AFHTTPSessionManager *session = [AFHTTPSessionManager manager];
    ///session.requestSerializer = [AFJSONRequestSerializer serializer];
    
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    params[@"curp"] = @"LOAA860421HDFPRL02";

    
    NSLog(@"DICCIONARIO: %@", jsonString);
//    
//    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
//        
//        if (!error) {
//            NSLog(@"Reply JSON: %@", responseObject);
//            
//            if ([responseObject isKindOfClass:[NSArray class]]) {
//                
//                NSLog(@"Response == %@",responseObject);
//                
//            }
//        } else {
//            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
//        }
//        
//    }]resume];
    
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObject:@"text/html"];
//    
//    [manager POST:url parameters:jsonString progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON Successsss: %@", responseObject);
//      
//    } failure:^(NSURLSessionTask *operation, NSError *error) {
//        NSLog(@"Error laaa: %@", error);
//    }];

    //AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [session POST:url parameters:jsonString progress:nil success:^(NSURLSessionTask *task, id responseObject){
        NSLog(@"JSON: %@", responseObject);
        
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
    
}








- (void) webService{

//    NSString *paramStr = [NSString stringWithFormat:
//                          @"tarjeta=%@"
//                          "&fechaNacimiento=%@"
//                          "&email=%@"
//                          "&key=%@"
//                          "&tipoMovil=%@",
//                          @"1234567890123",
//                          @"21/04/1986",
//                          @"as@mail.com",
//                          @"1d93235283cb3a61d04b7e6ce7d3b97f",
//                          @"iOS"];
    
    NSString *paramStr = [NSString stringWithFormat:@"<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                             "<soap:Envelope xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">\n"
                             "<soap:Body>\n"
                             "<ValidaTarjeta xmlns=\"http://tempuri.org/\">\n"
                             "<tarjeta>%@</tarjeta>\n"
                             "<fecnacimiento>%@</fecnacimiento>\n"
                             "<idmovil>%@</idmovil>\n"
                             "<tipomovil>%@</tipomovil>\n"
                             "<key>%@</key>\n"
                             "</ValidaTarjeta>\n"
                             "</soap:Body>\n"
                             "</soap:Envelope>\n"
                             ,@"7603000000006",
                             @"21/04/1986",
                             @"as@mail.com",
                             @"iOS",
                             @"1d93235283cb3a61d04b7e6ce7d3b97f"];
    
    
    NSString *url = @"https://www.appcacto.com:4450/MobileAppLiomont/appLiomont.asmx";
    [url stringByAppendingString:paramStr];
  
    
    NSLog(@"dictionary %@",paramStr);
    NSDictionary *responseData = [JSONHelper parseJsonResponse:url];
    NSLog(@"dictionary url %@",responseData);
    if (responseData) {
        NSString *md5Text = [responseData valueForKey:@"<mensaje>%@</mensaje>"];
        NSString *popupText = [NSString stringWithFormat:@"Md5 of %@ is %@", @"", md5Text];
        UIAlertView *alertMessage = [[UIAlertView alloc] initWithTitle:@"MD5 Text" message:popupText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertMessage show];
      
    }
    else
    {
        NSString *md5Text = [responseData valueForKey:@"<mensaje>%@</mensaje>"];
        NSString *popupText = [NSString stringWithFormat:@"%@", md5Text];
        UIAlertView *error = [[UIAlertView alloc] initWithTitle:@"Error" message: popupText delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [error show];
    
    }
    NSLog(@"dictionary data %@",responseData);
}


@end
