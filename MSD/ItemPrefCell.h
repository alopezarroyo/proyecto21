//
//  ItemPrefCell.h
//  MSD
//
//  Created by Rene Cabañas Lopez on 11/01/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ItemPrefCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *labelText;


@end
