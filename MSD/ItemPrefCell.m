//
//  ItemPrefCell.m
//  MSD
//
//  Created by Rene Cabañas Lopez on 11/01/2017.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "ItemPrefCell.h"

@implementation ItemPrefCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
