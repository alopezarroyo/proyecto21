//
//  CatalogoVC.m
//  MSD
//
//  Created by Lightsoft on 16/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "CatalogoVC.h"
#import "UIViewController+LMSideBarController.h"
#import "ItemView.h"

@interface CatalogoVC ()

@end

@implementation CatalogoVC


@synthesize titleStr;
@synthesize scrollView;
@synthesize pageControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    
    
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    
    for(int i=0;i<6;i++)
    {

        
        
        CGRect frame = CGRectMake((scrollView.frame.size.width)*i, 0, 246, 440);
        
        ItemView *itemView = [[ItemView alloc] initWithFrame:frame];
        
        [scrollView addSubview:itemView];
        
    }
    
    
    [self.pageControl setNumberOfPages:6];
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * 6,440);
    
}

- (void)scrollViewDidScroll:(UIScrollView *)_scrollView{
    
    CGFloat viewWidth = _scrollView.frame.size.width;
    // content offset - tells by how much the scroll view has scrolled.
    
    int pageNumber = floor((_scrollView.contentOffset.x - viewWidth/50) / viewWidth) +1;
    
    pageControl.currentPage=pageNumber;
    
}

- (void)pageChanged {
    
    int pageNumber = (int) pageControl.currentPage;
    
    CGRect frame = scrollView.frame;
    frame.origin.x = frame.size.width*pageNumber;
    frame.origin.y=0;
    
    [scrollView scrollRectToVisible:frame animated:YES];
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    self.title=self.titleStr;
    
    NSLog(@"titleView%@",self.titleStr);
    
}

- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}

@end
