//
//  TwoTableView.m
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 17/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import "TwoTableView.h"

@implementation TwoTableView

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
