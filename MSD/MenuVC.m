//
//  MenuVC.m
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import "MenuVC.h"
#import "PerfilVC.h"
#import "GeoLocVC.h"
#import "CatalogoVC.h"
#import "RegistroVC.h"
#import "CuentaVC.h"
#import "HistorialVC.h"
#import "UIViewController+LMSideBarController.h"
#import "MainNC.h"

@interface MenuVC ()
@property (weak, nonatomic) IBOutlet UIView *btn1;
@property (weak, nonatomic) IBOutlet UIView *btn2;
@property (weak, nonatomic) IBOutlet UIView *btn3;
@property (weak, nonatomic) IBOutlet UIView *btn4;

@property (weak, nonatomic) IBOutlet UIView *viewDetail;
@property (weak, nonatomic) IBOutlet UITableView  *tableBenefits;
@property (weak, nonatomic) IBOutlet UITextView   *textViewCOntact;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollDetail;



@property (weak, nonatomic) IBOutlet UIView *viewContent;


@property (weak, nonatomic) IBOutlet UIImageView *bannerDetail;
@property (weak, nonatomic) IBOutlet UIImageView *buttonHeader;
@property (weak, nonatomic) IBOutlet UILabel *titleDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgDetail;
@property (weak, nonatomic) IBOutlet UITextView *textViewDetail;
@property (weak, nonatomic) IBOutlet UILabel *subTitleDetail;
@property (weak, nonatomic) IBOutlet UIImageView *imgProximamente;

@end

@implementation MenuVC


- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title=@"";
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    self.navigationItem.leftBarButtonItem.enabled = NO;
    
   
    
   /*
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;

    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    */
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    self.btn1.clipsToBounds = YES;
    self.btn1.layer.cornerRadius = self.btn1.frame.size.height/2;
    self.btn1.layer.borderColor = [UIColor clearColor].CGColor;
    self.btn1.layer.borderWidth = 1.0f;
    self.btn1.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.btn1.layer.shouldRasterize = YES;
    
    self.btn2.clipsToBounds = YES;
    self.btn2.layer.cornerRadius = self.btn1.frame.size.height/2;
    self.btn2.layer.borderColor = [UIColor clearColor].CGColor;
    self.btn2.layer.borderWidth = 1.0f;
    self.btn2.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.btn2.layer.shouldRasterize = YES;
    
    self.btn3.clipsToBounds = YES;
    self.btn3.layer.cornerRadius = self.btn1.frame.size.height/2;
    self.btn3.layer.borderColor = [UIColor clearColor].CGColor;
    self.btn3.layer.borderWidth = 1.0f;
    self.btn3.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.btn3.layer.shouldRasterize = YES;
    
    self.btn4.clipsToBounds = YES;
    self.btn4.layer.cornerRadius = self.btn1.frame.size.height/2;
    self.btn4.layer.borderColor = [UIColor clearColor].CGColor;
    self.btn4.layer.borderWidth = 1.0f;
    self.btn4.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.btn4.layer.shouldRasterize = YES;
    
    
    UITapGestureRecognizer *singleFingerTapBtn1 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTapBtn1:)];
    singleFingerTapBtn1.numberOfTapsRequired = 1;
    [self.btn1 addGestureRecognizer:singleFingerTapBtn1];

    UITapGestureRecognizer *singleFingerTapBtn2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTapBtn2:)];
    singleFingerTapBtn2.numberOfTapsRequired = 1;
    [self.btn2 addGestureRecognizer:singleFingerTapBtn2];
    
    UITapGestureRecognizer *singleFingerTapBtn3 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTapBtn3:)];
    singleFingerTapBtn3.numberOfTapsRequired = 1;
    [self.btn3 addGestureRecognizer:singleFingerTapBtn3];
    
    UITapGestureRecognizer *singleFingerTapBtn4 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showTapBtn4:)];
    singleFingerTapBtn4.numberOfTapsRequired = 1;
    [self.btn4 addGestureRecognizer:singleFingerTapBtn4];
   
}


//The event handling method
- (void)showTapBtn1:(UITapGestureRecognizer *)recognizer {

    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.viewDetail.frame  = CGRectMake(0, self.view.frame.origin.y+65, self.viewDetail.frame.size.width,self.viewDetail.frame.size.height);
        _titleDetail.text = @"Proyecto 21";
        _bannerDetail.image =  [UIImage imageNamed:@"icono_novedades.png"];
        _imgDetail.image =  [UIImage imageNamed:@"fotonovedades.png"];
        self.scrollDetail.hidden = false;
        self.tableBenefits.hidden = true;
        self.textViewCOntact.hidden = true;
        self.imgProximamente.hidden = true;
        self.textViewDetail.text = @"Proyecto 21 es la llave que te ayudará a reducir y controlar tu peso, ayudándote a romper la barrera de los 21 días para adoptar los hábitos necesarios que te hagan llevar una vida saludable. Tu proyecto de salud inicia con tu especialista, ya sea a través de nuestros módulos o con tu médico de cabecera inscrito al programa. Él te brindará un diagnóstico personal y una prescripción médica, además apoyará tu tratamiento con recomendaciones de alimentación y ejercicio. Es ahí donde Proyecto 21 se asegurará que esas recomendaciones se conviertan en los hábitos de tu nueva vida.";
        self.subTitleDetail.text = @"Tu Proyecto de Vida";
    } completion:^(BOOL finished) {
        
        
    }];
    
    //Do stuff here...
}
- (IBAction)recetarioButton:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segueToInfo" sender:self];
}

//The event handling method
- (void)showTapBtn2:(UITapGestureRecognizer *)recognizer {
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.viewDetail.frame  = CGRectMake(0, self.view.frame.origin.y+65, self.viewDetail.frame.size.width,self.viewDetail.frame.size.height);
    
        _titleDetail.text = @"Recetario";
        _bannerDetail.image =  [UIImage imageNamed:@"icono_beneficios.png"];
        self.scrollDetail.hidden = true;
        self.tableBenefits.hidden = true;
        self.textViewCOntact.hidden = true;
        self.imgProximamente.hidden = false;
    } completion:^(BOOL finished) {
        
    }];
    
    //Do stuff here...
}

//The event handling method
- (void)showTapBtn3:(UITapGestureRecognizer *)recognizer {
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.viewDetail.frame  = CGRectMake(0, self.view.frame.origin.y+65, self.viewDetail.frame.size.width,self.viewDetail.frame.size.height);
        _titleDetail.text = @"Salud";
        _bannerDetail.image =  [UIImage imageNamed:@"icono_salud.png"];
        _imgDetail.image =  [UIImage imageNamed:@"fotosalud.png"];
        self.scrollDetail.hidden = false;
        self.tableBenefits.hidden = true;
        self.textViewCOntact.hidden = true;
        self.imgProximamente.hidden = true;
        self.textViewDetail.text = @"El sobrepeso y la obesidad se definen como una acumulación anormal o excesiva de grasa que puede ser perjudicial para la salud, poniendo a los pacientes en alto riesgo de presentar comorbilidades como diabetes mellitus, enfermedad cardiovascular y cáncer. En México, 70% de la población padece de sobrepeso y casi una tercera parte sufre de obesidad. Es por eso que debes acercarte a tu especialista y adoptar un nuevo estilo de vida.";
        self.subTitleDetail.text = @"Sobrepeso y Obesidad";
        
    } completion:^(BOOL finished) {
       
    }];
    
    //Do stuff here...
}//The event handling method
- (void)showTapBtn4:(UITapGestureRecognizer *)recognizer {
    
   
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.viewDetail.frame  = CGRectMake(0, self.view.frame.origin.y+65, self.viewDetail.frame.size.width,self.viewDetail.frame.size.height);
        _titleDetail.text = @"Contacto";
        _bannerDetail.image =  [UIImage imageNamed:@"icono_contacto.png"];
        self.scrollDetail.hidden = true;
        self.tableBenefits.hidden = true;
        self.textViewCOntact.hidden = false;
        self.imgProximamente.hidden = true;
        
        NSString *htmlString = @"<html><body><br></br><p style=font-family: arial, serif; font-size:14pt; font-style:italic><strong>Para mayor información del programa Proyecto 21 llama a nuestro Centro de Atención:</strong><br></br>Número<br></br>Tel:55.2629.5189</body></html>";
        NSAttributedString *attributedString = [[NSAttributedString alloc]
                                                initWithData: [htmlString dataUsingEncoding:NSUnicodeStringEncoding]
                                                options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType }
                                                documentAttributes: nil
                                                error: nil
                                                ];
        self.textViewCOntact.attributedText = attributedString;
        
    } completion:^(BOOL finished) {
        
    }];
    
    //Do stuff here...
}

-(IBAction)showForm:(id)sender{
    
    MainNC *mainNavigationController = (MainNC *)self.sideBarController.contentViewController;
    [mainNavigationController showRegisterVCController];
    [self.sideBarController hideMenuViewController:YES];
}

-(IBAction)hideDetail:(id)sender{
    
    [UIView animateWithDuration:.5 delay:0.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.viewDetail.frame  = CGRectMake(0, self.view.frame.size.height, self.viewDetail.frame.size.width,self.viewDetail.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ItemPromoCell"];
    
    
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}


-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /*
    if(isAppeared)
    {
        
    }
    else
    {
        isAppeared=YES;
        navBar = self.navigationController.navigationBar;
        [navBar addSubview:logoImg];
        [navBar addSubview:menuBtn];
        [menuBtn addTarget:self action:@selector(showMenu:) forControlEvents:UIControlEventTouchUpInside];
        NSLog(@"*******");
        
    }
     */
}

-(IBAction)showMenu:(id)sender
{
    self.menuVista.hidden=!self.menuVista.hidden;
}

-(IBAction)perfil:(id)sender
{
    PerfilVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"PerfilVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

-(IBAction)geoloc:(id)sender
{
    GeoLocVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"GeoLocVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

-(IBAction)catalogo:(id)sender
{
    CatalogoVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"CatalogoVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

-(IBAction)registro:(id)sender
{
    RegisterVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"RegistroVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

-(IBAction)cuenta:(id)sender
{
    CuentaVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"CuentaVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

-(IBAction)historial:(id)sender
{
    HistorialVC *vista=[self.storyboard instantiateViewControllerWithIdentifier:@"HistorialVC"];
    [self.navigationController pushViewController:vista animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
