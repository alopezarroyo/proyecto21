//
//  LMOthersViewController.m
//  LMSideBarControllerDemo
//
//  Created by LMinh on 10/23/16.
//  Copyright © 2016 LMinh. All rights reserved.
//

#import "LMOthersViewController.h"
#import "UIViewController+LMSideBarController.h"

@implementation LMOthersViewController


@synthesize titleStr;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    //menuV=[[MenuV alloc]initWithFrame:CGRectMake(0, 64, self.view.frame.size.width, self.view.frame.size.height-64)];
    //[menuV show];
    //[self.view addSubview:menuV];
    //self.menuVista.hidden=YES;
    
    /*
    UIImage* image3 = [UIImage imageNamed:@"logo_header.png"];
    CGRect frameimg = CGRectMake(0, -4,120, 40);
    UIButton *someButton = [[UIButton alloc] initWithFrame:frameimg];
    someButton.enabled = false;
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    UIBarButtonItem *mailbutton =[[UIBarButtonItem alloc] initWithCustomView:someButton];
    self.navigationItem.leftBarButtonItem=mailbutton;
    */
    self.navigationController.navigationBar.backgroundColor =  [UIColor clearColor];// [UIImage imageNamed:@"header.png"];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
  
    //self.textView.contentSize = CGSizeMake(3190, 4995);
    
}




-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.textView scrollRangeToVisible:NSMakeRange(300, 4000)];
    self.title=self.titleStr;

    NSLog(@"titleView%@",self.titleStr);

}


- (IBAction)rightMenuButtonTapped:(id)sender
{
    [self.sideBarController showMenuViewControllerInDirection:LMSideBarControllerDirectionRight];
}
@end
