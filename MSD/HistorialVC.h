//
//  HistorialVC.h
//  MSD
//
//  Created by Lightsoft on 17/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistorialVC : UIViewController<UITableViewDelegate,UITableViewDelegate>{
    NSString *titleStr;

}


@property(strong,nonatomic) NSString *titleStr;


@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
