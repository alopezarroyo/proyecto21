//
//  ThreeTableView.h
//  MSD
//
//  Created by Alejandro Lopez Arroyo on 17/05/17.
//  Copyright © 2017 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ThreeTableView : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageThree;
@property (weak, nonatomic) IBOutlet UILabel *labelTitleA;
@property (weak, nonatomic) IBOutlet UILabel *labelDescriptionA;


@end
