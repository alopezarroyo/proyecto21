//
//  AppDelegate.h
//  MSD
//
//  Created by Lightsoft on 04/12/15.
//  Copyright © 2015 Lightsoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

